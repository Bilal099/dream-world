<?php

namespace Database\Factories;

use App\Models\SupplyChain;
use Illuminate\Database\Eloquent\Factories\Factory;

class SupplyChainFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SupplyChain::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $amount = $this->faker->numerify('#####');
        return [
            'date'                          => now(),
            'cheque_name'                   => $this->faker->name,
            'cnic_number'                   => $this->faker->numerify('#####-#######-#'),
            'item_code'                     => $this->faker->numerify('###'),
            'work_of_nature'                => $this->faker->sentence(1),
            'purchaser_person'              => $this->faker->sentence(1),
            'total_amount'                  => $amount,
            'remaining_amount'              => $amount,
            'category_id'                   => 1,
            // 'payment_type_id'               => $this->faker->numerify('#'),
            // 'director_id'                   => $this->faker->numerify('#'),
            // 'approved_amount_by_director'   => $this->faker->numerify('####'),
            // 'accountant_id'                 => $this->faker->numerify('#'),
            // 'approved_amount_by_accountant' => $this->faker->numerify('####'),
            'status'                        => 0,
        ];
    }
}

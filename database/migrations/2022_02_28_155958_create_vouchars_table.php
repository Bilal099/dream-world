<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucharsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchars', function (Blueprint $table) {
            $table->id();
            $table->foreignId('payment_id')->references('id')->on('payments');
            $table->date('vouchar_date')->nullable();
            $table->string('VCH', 10)->nullable();
            $table->decimal('en_no', 15, 3)->nullable();
            $table->text('narration')->nullable();
            $table->decimal('total_credit', 15, 3)->nullable();
            $table->decimal('total_debit', 15, 3)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // [Owner] [nvarchar](35) NULL,
        // [Prn] [float] NULL,
        // [watch] [nvarchar](1) NULL,
        // [Status] [nchar](10) NULL,
        // [User1] [nchar](50) NULL,
        // [DeletedStatus] [char](10) NULL,
        // [InstEntryNo] [float] NULL,
        // [ReceiptID] [float] NULL,
        // [PRVVch] [char](10) NULL,
        // [PRVNo] [float] NULL,
        // [APLStatus] [nchar](10) NULL,
        // [APLSNo] [int] NULL,
        // [PostDated] [datetime] NULL,
        // [CheckNo] [nchar](10) NULL,
        // [TrStatus] [char](3) NULL,
        // [posslipno] [int] NULL,
        // [miscpayid] [int] NULL,
        // [resno] [int] NULL,
        // [locationid] [nvarchar](100) NULL,
        // [eventid] [int] NULL,
        // CONSTRAINT [PK_Vouchar] PRIMARY KEY CLUSTERED 
       
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchars');
    }
}

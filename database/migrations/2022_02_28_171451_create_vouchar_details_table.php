<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucharDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchar_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vouchar_id')->nullable()->references('id')->on('vouchars');
            $table->bigInteger('vendor_id')->nullable();
            $table->string('VCH', 10)->nullable();
            $table->decimal('en_no', 15, 3)->nullable();
            $table->string('ent_rem', 10)->nullable();
            $table->string('it_code')->nullable();
            $table->decimal('total_credit', 15, 3)->nullable();
            $table->decimal('total_debit', 15, 3)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchar_details');
    }
}

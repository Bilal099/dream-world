<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->integer('supply_chain_id')->nullable();
            $table->bigInteger('amount_payable')->nullable()->comment('Director`s appproved amount');
            $table->bigInteger('amount_paid')->nullable()->comment('Amount paid to vendor');
            $table->tinyInteger('mode_of_payment')->nullable();
            $table->tinyInteger('state')->nullable()->comment('0=Approved by Director, 1=Approved by Accountant');
            $table->tinyInteger('status')->nullable()->comment('0=In process, 1=Complete, 2=Rejected');
            $table->text('reason')->nullable();
            $table->tinyInteger('created_by')->nullable();
            $table->tinyInteger('updated_by')->nullable();
            $table->tinyInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SupplyChain;

class SupplyChainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SupplyChain::factory()
            ->count(10)
            ->create();
        // factory([App\Models\SupplyChain::class],amount: 10)->create();
    }
}

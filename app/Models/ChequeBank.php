<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChequeBank extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'bank_name',
        'blade_name',
        'default',
        'created_by',
        'updated_by',
        'deleted_by',
    ];
}

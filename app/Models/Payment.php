<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'supply_chain_id',
        'vendor_id',
        'amount_payable',
        'amount_paid',
        'mode_of_payment',
        'state',
        'status',
        'reason',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function paymentLog()
    {
        return $this->hasMany('App\Models\PaymentLog', 'payment_id', 'id')->where('reviewer_role','director');
    }

    public function modeOfPayment()
    {
        return $this->belongsTo('App\Models\PaymentType', 'mode_of_payment', 'id');
    }

    /**
     * Get the Vendor that owns the Payment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'id');
    }
}

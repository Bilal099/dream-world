<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'it_code',
        'cnic',
        'tax_rate',
        'payment_type_id',
        'category_id',
        'cheque_name',
        'department_id',
        'created_by',
        'updated_by',
        'deleted_by',
        'account_category_id',
        'sub_category_id',
        'sub_code',
        'ntn',
    ];

    /**
     * Get the user that owns the Vendor
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function modeOfPayment()
    {
        return $this->belongsTo(PaymentType::class, 'payment_type_id', 'id');
    }
    
    public function subAccountCategory()
    {
        return $this->belongsTo(SubAccountCategory::class, 'sub_category_id', 'id');
    }

    public function supplyChain()
    {
        return $this->hasMany(SupplyChain::class, 'vendor_id', 'id');
    }
    
    public function latestSupplyChain()
    {
        $temp = $this->hasMany(SupplyChain::class, 'vendor_id', 'id'); //->latest('id')->first();
        // dd($temp->work_of_nature);
        return $temp->latest('id')->first();
    }

    public function payment()
    {
        return $this->hasMany(Payment::class, 'vendor_id', 'id');
    }
    
    public function latestPayment()
    {
        return $this->hasMany(Payment::class, 'vendor_id', 'id')->latest('id');
    }
    
    public function paymentIsComplete()
    {
        return $this->hasMany(Payment::class, 'vendor_id', 'id')->where('status',1);
    }
}

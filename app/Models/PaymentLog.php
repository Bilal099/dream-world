<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentLog extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'payment_id',
        'review_date',
        'reviewer',
        'reason',
        'status',
        'reviewer_role',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function director()
    {
        return $this->belongsTo('App\Models\Director', 'reviewer', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Director', 'reviewer', 'id');
    }
}

<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Services\GoogleRecaptchaService;
class Recaptcha implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $service = new GoogleRecaptchaService();
        return $service->validate($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Captcha verification failed';
    }
}

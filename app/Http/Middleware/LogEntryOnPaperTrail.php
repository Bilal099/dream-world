<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Monolog\Logger;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\SyslogUdpHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class LogEntryOnPaperTrail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // create a log channel
        // $log = new Logger('Database');
        // $output = "%channel%.%level_name%: %message%";
        // $formatter = new LineFormatter($output);
        // $syslogHandler = new SyslogUdpHandler(env("PAPERTRAIL_URL"), env("PAPERTRAIL_PORT"));
        // $syslogHandler->setFormatter($formatter);
        // $log->pushHandler($syslogHandler);
        // // add records to the log
        // DB::listen(function($query)  use(&$log, &$request) {
        //     $userDetail = "Not Log in yet";
        //     if (Auth::check()) {
        //         $userDetail = [
        //             "id"                => Auth::user()->id,
        //             "name"              => Auth::user()->name,
        //             "email"             => Auth::user()->email,
        //             "ip"                => $request->ip(),
        //             "os"                => $this->getOS(),
        //             "browser"           => $this->getBrowser(),
        //             "pysicalAddress"    => $this->getPysicalAddress($this->getOS()),
        //         ];
        //     }
        //     $log->info(json_encode([
        //         "userDetail" => $userDetail,
        //         "routeDetail"=> [
        //             "url"   => $request->url(),
        //             "name"  => Route::currentRouteName()
        //         ],
        //         "requestMethod" => $request->method(),
        //         "requestDetail" => $request->all(),
        //         "databaseInteraction" => [
        //             "query"         => $query->sql,
        //             "bindings"      => $query->bindings,
        //             "responseTime"  => $query->time
        //         ],
        //         "dateTime"      => date("D, d M Y H:i:s")
        //     ]));
        // });
        // $log->error('************');
        
        return $next($request);
    }

    public function getOS()
    {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $os_platform  = "Unknown OS Platform";

        $os_array     = array(
            '/windows nt 10/i'      =>  'Windows 10',
            '/windows nt 6.3/i'     =>  'Windows 8.1',
            '/windows nt 6.2/i'     =>  'Windows 8',
            '/windows nt 6.1/i'     =>  'Windows 7',
            '/windows nt 6.0/i'     =>  'Windows Vista',
            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
            '/windows nt 5.1/i'     =>  'Windows XP',
            '/windows xp/i'         =>  'Windows XP',
            '/windows nt 5.0/i'     =>  'Windows 2000',
            '/windows me/i'         =>  'Windows ME',
            '/win98/i'              =>  'Windows 98',
            '/win95/i'              =>  'Windows 95',
            '/win16/i'              =>  'Windows 3.11',
            '/macintosh|mac os x/i' =>  'Mac OS X',
            '/mac_powerpc/i'        =>  'Mac OS 9',
            '/linux/i'              =>  'Linux',
            '/ubuntu/i'             =>  'Ubuntu',
            '/iphone/i'             =>  'iPhone',
            '/ipod/i'               =>  'iPod',
            '/ipad/i'               =>  'iPad',
            '/android/i'            =>  'Android',
            '/blackberry/i'         =>  'BlackBerry',
            '/webos/i'              =>  'Mobile'
        );

        foreach ($os_array as $regex => $value)
        {
            if (preg_match($regex, $user_agent))
            {
                $os_platform = $value;
            }
        }

        return $os_platform;
    }

    public function getBrowser() 
    {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
    
        $browser        = "Unknown Browser";
    
        $browser_array = array(
                                '/msie/i'      => 'Internet Explorer',
                                '/firefox/i'   => 'Firefox',
                                '/safari/i'    => 'Safari',
                                '/chrome/i'    => 'Chrome',
                                '/edge/i'      => 'Edge',
                                '/opera/i'     => 'Opera',
                                '/netscape/i'  => 'Netscape',
                                '/maxthon/i'   => 'Maxthon',
                                '/konqueror/i' => 'Konqueror',
                                '/mobile/i'    => 'Handheld Browser'
                         );
    
        foreach ($browser_array as $regex => $value)
        {
            if (preg_match($regex, $user_agent))
            {
                $browser = $value;
            }
        }
            
    
        return $browser;
    }

    public function getPysicalAddress($os)
    {
        if (str_contains($os, 'Windows')) 
        { 
            $physical_address   = explode(" ",exec('getmac'));
            $physical_address   = $physical_address[0];
            // $ipconfig           = explode("\n",shell_exec('ipconfig'));
            // $ipV6               = explode(". :",$ipconfig[8]); 
        }
        elseif(str_contains($os, 'Mac'))
        {
            $physical_address_command   = "networksetup -listallhardwareports";
            $command                    = "/sbin/ifconfig";

            exec($physical_address_command, $physical_address_output);
            $physical_address   = explode(": ",$physical_address_output[3]);
            $physical_address   = $physical_address[1];

            // exec($command, $output);
            // $ifconfig   = explode(" ",$output[14]);
            // $ipV6       = $ifconfig[1];
        }
        return $physical_address;
    }
}

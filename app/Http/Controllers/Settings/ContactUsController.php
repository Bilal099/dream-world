<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\ContactUsRepositoryInterface;
use App\Repository\SettingsRepositoryInterface;

use App\Mail\ContactUsEmail;
use App\Mail\ContactUsResponse;
use Illuminate\Support\Facades\Mail;
use Config;
use Artisan;

class ContactUsController extends Controller
{
    private $contactUsRepository;
    private $settingsRepository;
    
    public function __construct(ContactUsRepositoryInterface $contactUsRepository,SettingsRepositoryInterface $settingsRepository)
    {
        $this->contactUsRepository = $contactUsRepository;
        $this->settingsRepository  = $settingsRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('website.pages.contact');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function AjaxCallForContactUs(Request $request)
    {
        // dd($request);
        $settingdetail = array();
        $attributes = array();
        $attributes = [
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message,
        ];
        // try {

            $object =  $this->contactUsRepository->create($attributes);


            // if($smpt!=null)
            {
                $tempSettingdetail = $this->settingsRepository->findSettingDetailsBySettingId($smpt->id);  
                foreach ($tempSettingdetail as $key => $value) 
                {
                    $settingdetail[$value->key] = $value->value;
                }   
                
                Mail::to(env('MAIL_FROM_ADDRESS'))->send(new ContactUsEmail($attributes));

            }

            // Mail::to($request->email)->send(new ContactUsResponse($ContactUs));
        // } 
        // catch (\Throwable $th) 
        // {
        //     return response()->json(['status'=>false, 'message' => "Oops! some issue occur." ]);
        // }
        return response()->json(['status'=>true, 'message' => "Your Message is successfully send!" ]);
    }
}

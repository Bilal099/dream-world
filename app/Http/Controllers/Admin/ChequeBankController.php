<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\ChequeBankRepositoryInterface;
use Auth;
use DB;
use Validator;
use Session;
use Redirect;

class ChequeBankController extends Controller
{
    private $chequeBankRepository;

    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(ChequeBankRepositoryInterface $chequeBankRepository)
    {
        $this->chequeBankRepository = $chequeBankRepository;

        // for permissions
        $this->permissionView       = 'department-view';
        $this->permissionCreate     = 'department-create';
        $this->permissionEdit       = 'department-edit';
        $this->permissionDelete     = 'department-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasPermissionTo($this->permissionView))
        {
            $data = $this->chequeBankRepository->allDesc();
            return view('admin.chequeBank.index',\compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasPermissionTo($this->permissionCreate))
        {
            return view('admin.chequeBank.create');
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'bank_name' =>  'required',
            'blade_name' =>  'required',
        ]);

        if($validator->fails())
        {
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        try {
            
            $attributes['bank_name'] = $request->bank_name;
            $attributes['blade_name'] = $request->blade_name;
            $attributes['default'] = $request->has('default')? 1:0;
            $user_id = Auth::user()->id;
            if(isset($request->id))
            {
                $attributes['updated_by'] = $user_id;
                $object = $this->chequeBankRepository->update($attributes,$request->id);
            }
            else{
                $attributes['created_by'] = $user_id;
                $this->chequeBankRepository->create($attributes);
            }
        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('cheque.index')->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionEdit))
        {
            $data = $this->chequeBankRepository->find($id);
            return view('admin.chequeBank.create',compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionDelete))
        {
            try {
                $object = $this->chequeBankRepository->find($id);
                if($object)
                {
                    $this->chequeBankRepository->delete($id);
                }
            } 
            catch (\Throwable $th) {
                return redirect()->back()->with('error','Some thing is wrong!');
            }
            return redirect()->back()->with('success','Data is Successfully Deleted!');
        }
        else
        {
            return view('errors.401');
        }
    }
}

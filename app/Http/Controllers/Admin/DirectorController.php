<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\DirectorRepositoryInterface;
use Auth;
use DB;
use Validator;
use Session;
use Redirect;

class DirectorController extends Controller
{
    private $directorRepository;

    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(DirectorRepositoryInterface $directorRepository)
    {
        $this->directorRepository = $directorRepository;

        // for permissions
        $this->permissionView       = 'director-view';
        $this->permissionCreate     = 'director-create';
        $this->permissionEdit       = 'director-edit';
        $this->permissionDelete     = 'director-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasPermissionTo($this->permissionView))
        {
            $data = $this->directorRepository->allDesc();
            return view('admin.directors.index',\compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasPermissionTo($this->permissionCreate))
        {
            return view('admin.directors.create');
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(),[
            'name' =>  'required',
        ]);

        if($validator->fails())
        {
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        try {
            
            $user_id = Auth::user()->id;
            if(isset($request->id))
            {
                $attributes = [
                    'name'          => $request->name,
                    'updated_by'    => $user_id,
                ];
                $object = $this->directorRepository->update($attributes,$request->id);
            }
            else{
                $attributes = [
                    'name'          => $request->name,
                    'created_by'    => $user_id,
                ];
                $this->directorRepository->create($attributes);
            }
            
        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('director.index')->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionEdit))
        {
            $data = $this->directorRepository->find($id);
            return view('admin.directors.create',compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionDelete))
        {
            // dd('delete');
            try {
                $object = $this->directorRepository->find($id);
                if($object)
                {
                    $this->directorRepository->delete($id);
                }
            } 
            catch (\Throwable $th) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Record is not deleted!',
                ]);
            }
            return response()->json([
                'status' => true,
                'msg' => 'Data is Successfully Deleted!',
            ]);
        }
        else
        {
            return view('errors.401');
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\DepartmentRepositoryInterface;
use Auth;
use DB;
use Validator;
use Session;
use Redirect;

class DepartmentController extends Controller
{
    private $departmentRepository;

    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(DepartmentRepositoryInterface $departmentRepository)
    {
        $this->departmentRepository = $departmentRepository;

        // for permissions
        $this->permissionView       = 'department-view';
        $this->permissionCreate     = 'department-create';
        $this->permissionEdit       = 'department-edit';
        $this->permissionDelete     = 'department-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasPermissionTo($this->permissionView))
        {
            $data = $this->departmentRepository->allDesc();
            return view('admin.departments.index',\compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasPermissionTo($this->permissionCreate))
        {
            return view('admin.departments.create');
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' =>  'required',
            'code' =>  'required',
        ]);

        if($validator->fails())
        {
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        try {
            
            $user_id = Auth::user()->id;
            if(isset($request->id))
            {
                $attributes = [
                    'name'          => $request->name,
                    'code'          => $request->code,
                    'updated_by'    => $user_id,
                ];
                $object = $this->departmentRepository->update($attributes,$request->id);
            }
            else{
                $attributes = [
                    'name'          => $request->name,
                    'code'          => $request->code,
                    'created_by'    => $user_id,
                ];
                $this->departmentRepository->create($attributes);
            }
            
        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('department.index')->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionEdit))
        {
            $data = $this->departmentRepository->find($id);
            return view('admin.departments.create',compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionDelete))
        {
            // dd('delete');
            try {
                $object = $this->departmentRepository->find($id);
                if($object)
                {
                    $this->departmentRepository->delete($id);
                }
            } 
            catch (\Throwable $th) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Record is not deleted!',
                ]);
            }
            return response()->json([
                'status' => true,
                'msg' => 'Data is Successfully Deleted!',
            ]);
        }
        else
        {
            return view('errors.401');
        }
    }
}

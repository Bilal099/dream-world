<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\BankRepositoryInterface;
use App\Repository\BankAccountRepositoryInterface;
use Auth;
use DB;
use Validator;
use Session;
use Redirect;

class BankAccountController extends Controller
{
    private $bankAccountRepository;
    private $bankRepository;

    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(BankRepositoryInterface $bankRepository, BankAccountRepositoryInterface $bankAccountRepository)
    {
        $this->bankAccountRepository = $bankAccountRepository;
        $this->bankRepository = $bankRepository;

        // for permissions
        $this->permissionView       = 'bank-account-view';
        $this->permissionCreate     = 'bank-account-create';
        $this->permissionEdit       = 'bank-account-edit';
        $this->permissionDelete     = 'bank-account-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasPermissionTo($this->permissionView))
        {
            $data = $this->bankAccountRepository->allDesc();
            return view('admin.bankAccounts.index',\compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasPermissionTo($this->permissionCreate))
        {
            $banks = $this->bankRepository->all();
            return view('admin.bankAccounts.create',compact('banks'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'bank_id'       =>  'required',
            'branch_code'   =>  'required',
            'account_no'    =>  'required',
        ],[
            'bank_id.required'      => 'This fields is required',
            'branch_code.required'  => 'This fields is required',
            'account_no.required'   => 'This fields is required',
        ]);

        if($validator->fails())
        {
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        try {
            
            $user_id = Auth::user()->id;
            if(isset($request->id))
            {
                $attributes = [
                    'bank_id'     => $request->bank_id,
                    'branch_code' => $request->branch_code,
                    'account_no'  => $request->account_no,
                    'updated_by'  => $user_id,
                ];
                $object = $this->bankAccountRepository->update($attributes,$request->id);
            }
            else{
                $attributes = [
                    'bank_id'     => $request->bank_id,
                    'branch_code' => $request->branch_code,
                    'account_no'  => $request->account_no,
                    'created_by'  => $user_id,
                ];
                $this->bankAccountRepository->create($attributes);
            }
            
        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('bankAccount.index')->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionEdit))
        {
            $data = $this->bankAccountRepository->find($id);
            $banks = $this->bankRepository->all();
            return view('admin.bankAccounts.create',compact('data','banks'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionDelete))
        {
            try {
                $object = $this->bankAccountRepository->find($id);
                if($object)
                {
                    $this->bankAccountRepository->delete($id);
                }
            } 
            catch (\Throwable $th) {
                return redirect()->back()->with('error','Some thing is wrong!');
            }
            return redirect()->back()->with('success','Data is Successfully Deleted!');
        }
        else
        {
            return view('errors.401');
        }
    }
}

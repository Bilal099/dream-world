<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\SupplyChainRepositoryInterface;
use App\Repository\VendorRepositoryInterface;
use App\Repository\PaymentTypeRepositoryInterface;
use App\Repository\DirectorRepositoryInterface;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\PaymentRepositoryInterface;
use App\Repository\PaymentLogRepositoryInterface;
use App\Repository\VoucharRepositoryInterface;
use App\Repository\VoucharDetailRepositoryInterface;
use App\Repository\DepartmentBankRepositoryInterface;
use App\Repository\ChequeBankRepositoryInterface;

use Auth;
use DB;
use Validator;
use Session;
use Redirect;
use PDF;

class VoucharController extends Controller
{
    private $supplyChainRepository;
    private $chequeBankRepository;
    private $departmentBankRepository;
    private $vendorRepository;
    private $paymentTypeRepository;
    private $directorRepository;
    private $categoryRepository;
    private $paymentRepository;
    private $paymentLogRepository;
    private $voucharRepository;
    private $voucharDetailRepository;



    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(
        PaymentLogRepositoryInterface $paymentLogRepository, 
        ChequeBankRepositoryInterface $chequeBankRepository,
        DepartmentBankRepositoryInterface $departmentBankRepository,
        VendorRepositoryInterface $vendorRepository, 
        PaymentRepositoryInterface $paymentRepository, 
        SupplyChainRepositoryInterface $supplyChainRepository,
        PaymentTypeRepositoryInterface $paymentTypeRepository,
        DirectorRepositoryInterface $directorRepository,
        CategoryRepositoryInterface $categoryRepository,
        VoucharRepositoryInterface $voucharRepository,
        VoucharDetailRepositoryInterface $voucharDetailRepository
    )
    {
        $this->supplyChainRepository    = $supplyChainRepository;
        $this->chequeBankRepository = $chequeBankRepository;
        $this->departmentBankRepository = $departmentBankRepository;
        $this->vendorRepository         = $vendorRepository;
        $this->paymentTypeRepository    = $paymentTypeRepository;
        $this->directorRepository       = $directorRepository;
        $this->categoryRepository       = $categoryRepository;
        $this->paymentRepository        = $paymentRepository;
        $this->paymentLogRepository     = $paymentLogRepository;
        $this->voucharRepository        = $voucharRepository;
        $this->voucharDetailRepository  = $voucharDetailRepository;

        // for permissions
        // $this->permissionView       = 'accountant-view';
        // $this->permissionCreate     = 'accountant-create';
        // $this->permissionEdit       = 'accountant-edit';
        // $this->permissionDelete     = 'accountant-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        if(isset($request->tax_amount))
        {
            $validator = Validator::make($request->all(),[
                'bank_vendor_id'          =>  'required',   
                'payable_tax_id'          =>  'required',   
            ]);
        }
        else{
            $validator = Validator::make($request->all(),[
                'bank_vendor_id'          =>  'required',   
            ]);
        }
        

        if($validator->fails())
        {
            // dd($validator);
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        try {
            DB::beginTransaction();
            $user_id = Auth::user()->id;
            $vouchar_attributes = [];
            $vouchar_attributes = [
                'narration'    => $request->narration,
                'updated_by'   => $user_id,
            ];
            $object = $this->voucharRepository->update($vouchar_attributes,$request->id);
            
            $vouchar = $this->voucharRepository->find($request->id);

            $vouchar_detail_attributes1 = [];
            $vouchar_detail_attributes1 = [
                'vouchar_id'    => $vouchar->id,
                'vendor_id'     => $vouchar->payment->vendor->id,
                'VCH'           => 'DV',
                'ent_rem'       => $request->description[0],
                'it_code'       => $vouchar->payment->vendor->it_code,
                'total_debit'  => $request->party_amount,
                'created_by'    => $user_id,
            ];
            $voucharDetail1 = $this->voucharDetailRepository->create($vouchar_detail_attributes1);
            
            // $vendor = $this->vendorRepository->find($request->vendor_id);
            $vendor = $this->vendorRepository->find($request->bank_vendor_id);
            $vouchar_detail_attributes2 = [];
            $vouchar_detail_attributes2 = [
                'vouchar_id'    => $vouchar->id,
                'vendor_id'     => $vendor->id,
                'VCH'           => 'DV',
                'ent_rem'       => $request->description[1],
                'it_code'       => $vendor->it_code,
                'total_credit'   => $request->party_amount,
                'created_by'    => $user_id,
            ];
            $voucharDetail2 = $this->voucharDetailRepository->create($vouchar_detail_attributes2);

            if($vouchar->payment->vendor->tax_rate != null)
            {
                $tax_vendor = $this->vendorRepository->find($request->payable_tax_id);

                
                $vouchar_detail_attributes3 = [];
                $vouchar_detail_attributes3 = [
                    'vouchar_id'    => $vouchar->id,
                    'vendor_id'     => $request->payable_tax_id,
                    'VCH'           => 'DV',
                    'ent_rem'       => $request->description[2],
                    'it_code'       => $tax_vendor->it_code,
                    'total_debit'   => $request->tax_amount,
                    'created_by'    => $user_id,
                ];
                $voucharDetail3 = $this->voucharDetailRepository->create($vouchar_detail_attributes3);
                
                // $vendor = $this->vendorRepository->find($request->vendor_id);
                $vouchar_detail_attributes4 = [];
                $vouchar_detail_attributes4 = [
                    'vouchar_id'    => $vouchar->id,
                    'vendor_id'     => $vendor->id,
                    'VCH'           => 'DV',
                    'ent_rem'       => $request->description[3],
                    'it_code'       => $vendor->it_code,
                    'total_credit'   => $request->tax_amount,
                    'created_by'    => $user_id,
                ];
                $voucharDetail4 = $this->voucharDetailRepository->create($vouchar_detail_attributes4);
                
                $vouchar_detail_attributes5 = [];
                $vouchar_detail_attributes5 = [
                    'vouchar_id'    => $vouchar->id,
                    'vendor_id'     => $vouchar->payment->vendor->id,
                    'VCH'           => 'JV',
                    'ent_rem'       => $request->description[4],
                    'it_code'       => $vouchar->payment->vendor->it_code,
                    'total_debit'   => $request->tax_amount,
                    'created_by'    => $user_id,
                ];
                $voucharDetail5 = $this->voucharDetailRepository->create($vouchar_detail_attributes5);
                
                $vouchar_detail_attributes6 = [];
                $vouchar_detail_attributes6 = [
                    'vouchar_id'    => $vouchar->id,
                    'vendor_id'     => $request->tax_record_id,
                    'VCH'           => 'JV',
                    'ent_rem'       => $request->description[5],
                    'it_code'       => $request->tax_record_id_it_code,
                    'total_credit'   => $request->tax_amount,
                    'created_by'    => $user_id,
                ];
                $voucharDetail6 = $this->voucharDetailRepository->create($vouchar_detail_attributes6);
            }

            
            DB::commit();
        } 
        catch (\Throwable $th) 
        {
            DB::rollback();
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('accountant.index')->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->voucharRepository->find($id);
        // dd($id,$data);
        return view('admin.vouchar.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department_id = null;
        $vendors = null;

        $data = $this->voucharRepository->find($id);

        if ($data != null) 
        {
            $department_id =  $data->payment->vendor->department_id;
            // $vendors = $this->vendorRepository->findByColumnMultiple('department_id',$department_id);
            $vendors = $this->departmentBankRepository->all();
        }
        // dd($data->voucharDetail);
        return view('admin.vouchar.edit',compact('data','vendors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all(),$id);
        
        $validator = Validator::make($request->all(),[
            'vendor_id'          =>  'required',   
        ]);

        if($validator->fails())
        {
            // dd($validator);
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        // try {
        //     //code...
        // } catch (\Throwable $th) {
        //     //throw $th;
        // }
        $user_id = Auth::user()->id;

        $vouchar_attributes = [
            'narration'    => $request->narration,
            'updated_by'   => $user_id,
        ];
        $object = $this->voucharRepository->update($vouchar_attributes,$id);

        foreach ($request->vouchar_detail_id as $key => $value) 
        {
            $vouchar_detail_attributes = [
                'vendor_id'     => @$request->vendor_id[$key],
                'ent_rem'       => @$request->description[$key],
                'it_code'       => @$request->it_code[$key],
                'total_credit'  => @$request->total_credit[$key],
                'total_debit'   => @$request->total_debit[$key],
                'updated_by'    => @$user_id,
            ];
            $voucharDetail4 = $this->voucharDetailRepository->update($vouchar_detail_attributes,$value);
        }

        return redirect()->route('accountant.index')->with('success','Data is Successfully Added');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createVouchar($id)
    {
        $department_id = null;
        $vendors = null;

        $data = $this->voucharRepository->find($id);

        if ($data != null) 
        {
            $department_id =  $data->payment->vendor->department_id;
            $vendors = $this->departmentBankRepository->all();
            // $vendors = $this->vendorRepository->findByColumnMultiple('department_id',$department_id);
        }
        return view('admin.vouchar.create',compact('data','vendors'));
    }

    public function chequeBlade($id)
    {
        // dd('bilal');
        $data = $this->voucharRepository->find($id);
        $cheques = $this->chequeBankRepository->all();
        // dd($id,$data);
        return view('admin.vouchar.show',compact('data','cheques'));
    }

    // public function viewChequeDetail($id)
    public function viewChequeDetail(Request $request)
    {
        // dd($request->all());

        $bank_cheque = $this->chequeBankRepository->find($request->bank_cheque);
        
        $blade = $bank_cheque->blade_name;
        $blade = 'admin.chequePrint.'.$blade;
        // dd($blade);
        $data = [
            // 'object' => $object,
            'date' => date('d m Y'),
            'amount' => $request->amount,
            'pay'   => $request->cheque_name,
        ];
        // $pdf = PDF::loadView('admin.chequePrint.temp', $data);
        $pdf = PDF::loadView($blade, $data);
        $customPaper = array(0,0,642.51,279.68);
        // return $pdf->setPaper($customPaper)->stream('Supply-Chain-'.now().'.pdf');
        // $pdf->setPaper($customPaper);
        // // $pdf->render();
        // $pdf->stream('temp-cheque-'.now().'.pdf');
        // return $pdf;
        // setPaper('catalog #10 1/2 envelope','portrait')
        return $pdf->setPaper($customPaper)->stream('Supply-Chain-'.now().'.pdf');
        // return $pdf->setPaper('catalog #10 1/2 envelope','Landscape')->stream('Supply-Chain-'.now().'.pdf');
    }
}

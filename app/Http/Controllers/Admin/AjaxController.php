<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\DepartmentRepositoryInterface;
use App\Repository\VendorRepositoryInterface;
use App\Repository\PaymentTypeRepositoryInterface;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\SubAccountCategoryRepositoryInterface;

use Auth;
use DB;
use Validator;
use Session;
use Redirect;

class AjaxController extends Controller
{
    private $departmentRepository;
    private $vendorRepository;
    private $paymentTypeRepository;
    private $categoryRepository;
    private $subAccountCategoryRepository;

    
    public function __construct(
        VendorRepositoryInterface $vendorRepository, 
        DepartmentRepositoryInterface $departmentRepository,
        PaymentTypeRepositoryInterface $paymentTypeRepository,
        CategoryRepositoryInterface $categoryRepository,
        SubAccountCategoryRepositoryInterface $subAccountCategoryRepository
    )
    {
        $this->departmentRepository     = $departmentRepository;
        $this->vendorRepository         = $vendorRepository;
        $this->paymentTypeRepository    = $paymentTypeRepository;
        $this->categoryRepository       = $categoryRepository;
        $this->subAccountCategoryRepository = $subAccountCategoryRepository;
    }

    public function getVendorByDepartment(Request $request)
    {
        try {
            // $data = $this->vendorRepository->findByColumnMultiple('department_id',$request->department_id);
            $data = $this->vendorRepository->where([['department_id','=',$request->department_id],['name', 'like', '%'.$request->value.'%']]);
            // dd($data);
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>true,
                'msg' => 'There is some issue.',
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $data,
        ]);
    }

    public function getVendorDetail(Request $request)
    {
        try {
            $data = $this->vendorRepository->find($request->id);
            // dd($data);
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>false,
                'msg' => 'There is some issue.',
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $data,
        ]);
    }

    public function getSubCategoryByDepartment(Request $request)
    {
        try {
            $data = $this->subAccountCategoryRepository->findByColumnMultiple('department_id',$request->department_id);
            // dd($data);
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>false,
                'msg' => 'There is some issue.',
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $data,
        ]);
    }
    
    public function getVendorBySubCategory(Request $request)
    {
        try {
            $data = $this->vendorRepository->findByColumnMultiple('sub_category_id',$request->sub_category_id);
            // dd($data);
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>false,
                'msg' => 'There is some issue.',
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $data,
        ]);
    }

    public function getVendorByName(Request $request)
    {
        // dd($request->all());
        try {
            $data = $this->vendorRepository->where([['name', 'like', '%'.$request->value.'%']]);
            // dd($data);
        } 
        catch (\Throwable $th) 
        {
            return response()->json([
                'status'=>true,
                'msg' => 'There is some issue.',
                'data'=> null,
            ]);
        }

        return response()->json([
            'status'=>true,
            'msg' => 'Record is successfully fetched',
            'data'=> $data,
        ]);
    }
}

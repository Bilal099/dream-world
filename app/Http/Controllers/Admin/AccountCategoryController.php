<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\AccountCategoryRepositoryInterface;
use App\Repository\DepartmentRepositoryInterface;
use App\Imports\AccountCategoriesImport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use DB;
use Validator;
use Session;
use Redirect;

class AccountCategoryController extends Controller
{
    private $accountCategoryRepository;
    private $departmentRepository;

    private $permissionView;
    private $permissionCreate;
    private $permissionEdit;
    private $permissionDelete;
    
    public function __construct(AccountCategoryRepositoryInterface $accountCategoryRepository, DepartmentRepositoryInterface $departmentRepository)
    {
        $this->accountCategoryRepository = $accountCategoryRepository;
        $this->departmentRepository = $departmentRepository;
    
        // for permissions
        $this->permissionView       = 'accountant-category-view';
        $this->permissionCreate     = 'accountant-category-create';
        $this->permissionEdit       = 'accountant-category-edit';
        $this->permissionDelete     = 'accountant-category-delete';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasPermissionTo($this->permissionView))
        {
            $data = $this->accountCategoryRepository->allDesc();
            return view('admin.accountCategories.index',\compact('data'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasPermissionTo($this->permissionCreate))
        {
            $department = $this->departmentRepository->all();
            return view('admin.accountCategories.create',compact('department'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'              =>  'required',
            'code'              => 'required',
            'department_id'     =>  'required',
        ],[
            // 'name.required'      => 'This fields is required',
        ]);

        if($validator->fails())
        {
            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        try {
            
            $attributes = [];
            DB::beginTransaction();
            $user_id = Auth::user()->id;
            if(isset($request->id))
            {
                $attributes = [
                    'name'          => $request->name,
                    'code'          => $request->code,
                    'department_id' => ($request->department_id!=null)? $request->department_id:null,
                    'updated_by'    => $user_id,
                ];
                $object = $this->accountCategoryRepository->update($attributes,$request->id);
            }
            else{
                $attributes = [
                    'name'          => $request->name,
                    'code'          => $request->code,
                    'department_id' => ($request->department_id!=null)? $request->department_id:null,
                    'created_by'    => $user_id,
                ];
                $this->accountCategoryRepository->create($attributes);
            }
            DB::commit();
        } 
        catch (\Throwable $th) 
        {
            DB::rollback();
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return redirect()->route('accountCategory.index')->with('success','Data is Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionEdit))
        {
            $data = $this->accountCategoryRepository->find($id);
            $department = $this->departmentRepository->all();
            return view('admin.accountCategories.create',compact('data','department'));
        }
        else
        {
            return view('errors.401');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->hasPermissionTo($this->permissionDelete))
        {
            try {
                $object = $this->accountCategoryRepository->find($id);
                if($object)
                {
                    $this->accountCategoryRepository->delete($id);
                }
            } 
            catch (\Throwable $th) {
                return redirect()->back()->with('error','Some thing is wrong!');
            }
            return redirect()->back()->with('success','Data is Successfully Deleted!');
        }
        else
        {
            return view('errors.401');
        }
    }

    public function importView()
    {
        $department = $this->departmentRepository->all();

        return view('admin.accountCategories.import',compact('department'));
    }
    public function import(Request $request)
    {
        try {
            DB::beginTransaction();
            Excel::import(new AccountCategoriesImport($request->department_id),request()->file('file'));
            DB::commit();
        } 
        catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error','File is not uploaded!');
        }

        return redirect()->back()->with('success','File is successfully uploaded!');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use Spatie\Permission\Models\Permission;
class PermissionsController extends Controller
{

    // public function create(){
    //     return view('admin.permissions.create');
    // }
    // public function store(Request $request){
    //     $request->validate([
    //         "permission" => "required|string",
    //     ]);
    //     Permission::create(['name' => 'edit articles']);
    //     return view();
    // }
    

    public function permissions($id)
    {
        $allPermissions = Permission::where('guard_name','admin')->get()->groupBy(function ($item, $key) {
            // return explode("-",$item->name)[0];
            $str = $item->name;
            $one_words = array("-view", "-create", "-edit", "-delete");
            $temp = $this->multiexplode($one_words,$str);
            return $temp;
        })->toArray();
        $users = Admin::with('roles')->get();
        $user = $users->reject(function ($userQuery, $key) use ($id) {
            return $userQuery->hasRole('superadmin');
        })->where('id',$id)->first();
        if($user != null)
        {
            if(count($user->permissions) > 0){
                $userPermissions = $user->permissions->toQuery()->pluck('name',"id")->toArray();
            }else{
                $userPermissions = [];
            }
            request()->session()->put('userId',$id);
            return view("admin.permissions.index",compact('userPermissions',"user","allPermissions"));
        }
        return abort(404);
    }
    public function setPermissions(Request $request)
    {
        $user = Admin::findOrFail(request()->session()->get('userId'));
        $user->syncPermissions();
        foreach ($request->except('_token') as $key => $value) {
            $user->givePermissionTo($key);
        }
        return redirect()->route('users.index')->withMessage("User Permission Changed Successfully.");
    }

    public function multiexplode ($delimiters,$string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }
}

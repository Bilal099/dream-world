<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\MetaTagRepositoryInterface;

use DB;
use Validator;
use Session;
use Redirect;

class MegaTagController extends Controller
{
    private $metaTagRepository;
    
    public function __construct(MetaTagRepositoryInterface $metaTagRepository)
    {
        $this->metaTagRepository       = $metaTagRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settingdetail = array();
        $temp = $this->metaTagRepository->all();
        $data = array();
        if($temp!=null)
        {  
            foreach ($temp->all() as $key => $value) 
            {
                $data[$value->key."_id"] = $value->id;
                $data[$value->key] = $value->content;
            }   
        }
        // dd($data);
        return view('admin.metaTags.create',\compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->tags['image']);
        // dd($request->all());
        // dd(!isset($request->id));
        if(!isset($request->id))
        {
            $validator = Validator::make($request->all(),[
                'tags.*'    => 'required',
            ],
            [
                'tags.*.required'    => 'This field is required',
            ]);

            if($validator->fails())
            {
                Session::put('msg', 'Please fill required feilds!');
                return Redirect::back()->withInput($request->input())->withErrors($validator)->with('msg','Please insert all images again!');
            }
        }

        $msg = '';
        try {
            if(isset($request->id))
            {
                $msg = 'Data is Successfully Updated';
                foreach ($request->tags as $key => $value) 
                {
                    $attributes = array();
                    $image_fields = ['image','logo','favicon' ];     
                    if(in_array($key,$image_fields))
                    {
                        $temp                   = $value->store('public/metaImage');
                        $attributes = array(
                            'key'       => $key,
                            'content'   => asset(str_replace("public/","storage/",$temp)),
                        );
                    }
                    else{
                        $attributes = array(
                            'key'       => $key,
                            'content'   => $value,
                        );
                    }
                    if($request->id[$key]!=null)
                    {
                        $object = $this->metaTagRepository->update($attributes,$request->id[$key]);
                    }
                    else{
                        $object = $this->metaTagRepository->create($attributes);
                    }
                }
            }
            else{

                $msg = 'Data is Successfully Added';
                foreach ($request->tags as $key => $value) 
                {
                    $attributes = array();     
                    if($key == 'image')
                    {
                        $temp                   = $value->store('public/metaImage');
                        $attributes = array(
                            'key'       => $key,
                            'content'   => asset(str_replace("public/","storage/",$temp)),
                        );
                    }
                    else{
                        $attributes = array(
                            'key'       => $key,
                            'content'   => $value,
                        );
                    }
                    $object = $this->metaTagRepository->create($attributes);
                }
            }
        } catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is wrong!');
        }
        return \redirect()->back()->with('success',$msg);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

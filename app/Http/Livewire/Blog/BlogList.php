<?php

namespace App\Http\Livewire\Blog;

use Livewire\Component;
use App\Repository\BlogRepositoryInterface;


class BlogList extends Component
{
    public $data;
    public $confirming;

    public function mount(BlogRepositoryInterface $blogRepository)
    {
        $this->data = $blogRepository->all();
    }
    public function render()
    {
        return view('livewire.blog.blog-list');
    }

    public function confirmDelete($id)
    {
        $this->confirming = $id;
    }

    public function delete($id,BlogRepositoryInterface $blogRepository)
    {
        $blogRepository->delete($id);
        session()->flash('success', 'Blog successfully deleted.');
        return redirect()->to('admin/blog');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Repository\FaqRepositoryInterface;
use App\Repository\LanguageRepositoryInterface;
use DB;


class FaqForm extends Component
{
    // Repository Variables
    private $faqRepository;
    private $languageRepository;

    // Global Variables
    public $_id;
    public $languages;
    public $tabActive;
    
    // Form Variables 
    public $question;
    public $answer;
    public $active;

    // Form Validations
    protected $rules = [
        'question.*'    => 'required|nullable',
        'answer.*'      => 'required|nullable',
    ];
    
    protected $messages = [
        'question.*.required' => 'This question cannot be empty.',
        'answer.*.required' => 'This answer cannot be empty.',
    ];
    
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function mount(FaqRepositoryInterface $faqRepository, LanguageRepositoryInterface $languageRepository, $id)
    {
        $this->languages = $languageRepository->all();
        $this->tabActive = $this->languages[0]->code;
        if($id){
            $this->_id = $id;
            $faq = $faqRepository->find($id);
            $this->active   = $faq->active;
        }
        foreach ($this->languages as $index => $language) {
            $this->question[$language->code]    = isset($faq)?$faq->translate(@$language->code)->question:"";
            $this->answer[$language->code]      = isset($faq)?$faq->translate(@$language->code)->answer:"";
        }
    }

    public function save(FaqRepositoryInterface $faqRepository)
    {
        if($this->checkEmpty()){
            $validatedData = $this->validate();
            $attributes = $this->setAttribute();
            $faqRepository->createFaq($attributes);
            $this->question = array();
            $this->answer = array();
            $this->active = 0;
            session()->flash('message', 'FAQ successfully created.');
        }
    }

    public function update(FaqRepositoryInterface $faqRepository)
    {
        if($this->checkEmpty()){
            $attributes = $this->setAttribute();
            $faqRepository->update($attributes,$this->_id);
            session()->flash('message', 'FAQ successfully Updated.');
        }
    }

    public function render()
    {
        return view('livewire.faq-form');
    }

    public function setAttribute()
    {
        $attributes = array();
        foreach ($this->languages as $key => $value){
            $attributes[$value->code] = array(
                'question'  => $this->question[$value->code],
                'answer'    => $this->answer[$value->code],
            );
        }
        $attributes['active'] = $this->active?1:0;
        return $attributes;
    }

    public function checkEmpty()
    {
        foreach ($this->languages as $key => $value){
            if (!isset($this->question[$value->code]) || !isset($this->answer[$value->code])) {
                session()->flash('validateError', 'All Field Should be Filled');
                return false;
            }
        }
        return true;
    }

    public function changeTabs($languageCode)
    {
        $this->tabActive = $languageCode;
    }
}

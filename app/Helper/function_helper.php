<?php


function actors()
{
  $actors = array(
    0 => "supplier",
    1 => "client",
    2 => "dyeing",
    3 => "bank",
    // 4 => "expense",
  );
  return $actors;
}


function print_b($print_pre){
    echo "<pre>";
    print_r($print_pre);
    echo "</pre>";
    die;
}

function custom_date_format($date,$format)
{
  $temp_date = date_create(@$date);
  return date_format($temp_date,$format); // "D jS M, Y"
}



function measurement_unit()
{
  $measurementUnit = array(
    0 => "KGS",
    1 => "MTRS",
    2 => "SQ.MTRS",
    3 => "FT",
    4 => "SQ.FT",
    5 => "LTRS",
    6 => "PRS",
    7 => "PCS",
    8 => "DZP",
    9 => "4PK"
  );
  return $measurementUnit;
}

function shippping_method()
{
  $shippping_method = array(
    
    0 => "By Sea",
    1 => "By Air",
    2 => "By Courier",
  );
  return $shippping_method;
}

function price_base()
{
  $price_base = array(
    
    0 => "EXW",
    1 => "FOB",
    2 => "CRF",
    3 => "CIF"
  );
  return $price_base;
}

function currency()
{
  $currency = array(
    
    0 => "USD",
    1 => "EUR",
    2 => "PKR",
    
  );
  return $currency;
}


function amountInWords(float $amount)
{
   $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
   // Check if there is any number after decimal
   $amt_hundred = null;
   $count_length = strlen($num);
   $x = 0;
   $string = array();
   $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
     3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
     7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
     10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
     13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
     16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
     19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
     40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
     70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
    $here_digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
    while( $x < $count_length ) {
      $get_divider = ($x == 2) ? 10 : 100;
      $amount = floor($num % $get_divider);
      $num = floor($num / $get_divider);
      $x += $get_divider == 10 ? 1 : 2;
      if ($amount) {
       $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
       $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
       $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.' 
       '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. ' 
       '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
        }
   else $string[] = null;
   }
   $implode_to_Rupees = implode('', array_reverse($string));
   $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . " 
   " . $change_words[$amount_after_decimal % 10]) . ' Paise' : '';
   return ($implode_to_Rupees ? $implode_to_Rupees . 'Rupees ' : '') . $get_paise;
}

?>

<?php

namespace App\Imports;

use App\Models\AccountCategory;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Auth;

class AccountCategoriesImport implements ToCollection
{
    private $department_id;
    public function __construct($department_id)
    {
        $this->department_id = $department_id;
    }
    
    public function collection(Collection $rows)
    {
        $userId = Auth::user()->id;
        $i = 0;
        foreach ($rows as $row) 
        {
            if ($row[0] != 'M_CODE') 
            {
                if(!AccountCategory::where([ ['name','=',$row[1]],['department_id','=',$this->department_id] ])->exists())
                {
                    $object = AccountCategory::create([
                        'name'              => $row[1],
                        'code'              => $row[0],
                        'department_id'     => $this->department_id,
                        'created_by'        => $userId,
                    ]);
                }
                
            }
        }
    }
}

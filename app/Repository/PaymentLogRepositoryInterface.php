<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface PaymentLogRepositoryInterface
{
    public function update(array $attributes,$id);
}
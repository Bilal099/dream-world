<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface ChequeBankRepositoryInterface
{
   public function update(array $attributes,$id);
}
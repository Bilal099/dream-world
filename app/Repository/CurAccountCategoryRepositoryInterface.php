<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface CurAccountCategoryRepositoryInterface
{

    public function update(array $attributes,$id);
}
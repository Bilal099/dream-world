<?php
namespace App\Repository;

use App\Models\Setting;
use Illuminate\Support\Collection;

interface SettingsRepositoryInterface
{

    public function update(array $attributes);

    public function delete($id);


    public function findSettingDetailsBySettingId($id);

    public function findSettingDetailsByCaptchaSettingId($id);

}
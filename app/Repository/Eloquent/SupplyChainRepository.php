<?php
namespace App\Repository\Eloquent;

use App\Models\SupplyChain;
use App\Repository\SupplyChainRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SupplyChainRepository extends BaseRepository implements SupplyChainRepositoryInterface
{

    public function __construct(SupplyChain $model)
    {
        parent::__construct($model);
    }

    public function update(array $attributes,$id)
    {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
    }

    public function getAllUnPaid()
    {
        $object = $this->model->where('status',1)->get();
        // dd($object);
        return $object;
    }

    public function getPaymentIsComplete()
    {
        // $object = DB::table('supply_chains')
        //     ->join('payments', 'supply_chains.id', '=', 'payments.supply_chain_id')
        //     ->select(
        //         'supply_chains.id',
        //         'supply_chains.date',
        //         'supply_chains.cheque_name',
        //         'supply_chains.cnic_number',
        //         'supply_chains.item_code',
        //         'supply_chains.work_of_nature',
        //         'supply_chains.purchaser_person',
        //         'supply_chains.total_amount',
        //         'supply_chains.remaining_amount',
        //         'supply_chains.category_id',
        //         'supply_chains.payment_type_id',
        //         'supply_chains.director_id',
        //         'supply_chains.approved_amount_by_director',
        //         'supply_chains.accountant_id',
        //         'supply_chains.approved_amount_by_accountant',
        //         'supply_chains.status',
        //         'payments.amount_paid',
        //         'payments.mode_of_payment',
        //         'SUM(p.amount_paid) AS paid_amount'
        //     )
        //     ->where('payments.status',1)
        //     ->groupBy('supply_chains.id')
        //     ->get();

        // $object = DB::select('SELECT sc.id, sc.date, sc.cheque_name, sc.cnic_number, sc.item_code, sc.work_of_nature, sc.purchaser_person, sc.total_amount, sc.remaining_amount, sc.category_id, sc.payment_type_id, sc.director_id, sc.approved_amount_by_director, sc.accountant_id, sc.approved_amount_by_accountant, sc.status, p.amount_paid, SUM(p.amount_paid) AS paid_amount , c.name AS category_name
        //     FROM supply_chains sc 
        //     LEFT JOIN payments p 
        //     ON sc.id = p.supply_chain_id 
        //     LEFT JOIN categories c
        //     ON sc.category_id = c.id
        //     WHERE p.status = 1 GROUP BY sc.id ');

        // paymentComplete

        // $object = $this->model->with('paymentComplete')->get();

        $object = $this->model->leftJoin('payments','supply_chains.id','=','payments.supply_chain_id')->where('payments.status','=',1)->groupBy('supply_chains.id')->get();
        // ->selectRaw("containers.id,UPPER(containers.name) as name,containers.size,itemtypes.name as type")->where('containers.name','LIKE','%'.$search.'%')->get();

        // dd($object[1]->paymentComplete);
        return $object;
    }

    public function getPaymentInProcess()
    {
        $object = DB::table('supply_chains')
            ->join('payments', 'supply_chains.id', '=', 'payments.supply_chain_id')
            ->select('supply_chains.*', 'payments.amount_paid','payments.mode_of_payment')
            ->where('payments.status',1)
            ->get();
        return $object;
    }

    public function getPaymentnotInProcess()
    {
        // $first = DB::table('supply_chains')
        //     ->join('categories', 'supply_chains.category_id', '=', 'categories.id')
        //     ->select('supply_chains.*','categories.name AS category_name')
        //     ->where('supply_chains.status',0);
        //     // ->get();
        
        // $object = DB::table('supply_chains')
        //     ->join('payments', 'supply_chains.id', '=', 'payments.supply_chain_id')
        //     ->join('categories', 'supply_chains.category_id', '=', 'categories.id')
        //     ->select('supply_chains.*','categories.name AS category_name')
        //     // ->select('supply_chains.*', 'payments.amount_paid','payments.mode_of_payment','categories.name AS category_name')
        //     ->where('payments.status',1)
        //     ->where('supply_chains.status',0)
        //     ->union($first)
        //     ->get();

        $object = $this->model->where('status',0)->get();
        return $object;
    }



}
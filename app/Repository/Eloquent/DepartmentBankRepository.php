<?php
namespace App\Repository\Eloquent;

use App\Models\DepartmentBank;
use App\Repository\DepartmentBankRepositoryInterface;
use Illuminate\Support\Collection;

class DepartmentBankRepository extends BaseRepository implements DepartmentBankRepositoryInterface
{
    public function __construct(DepartmentBank $model)
    {
        parent::__construct($model);
    }

    public function update(array $attributes,$id)
    {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
    }
}
<?php   

namespace App\Repository\Eloquent;   

use App\Repository\EloquentRepositoryInterface; 
use Illuminate\Database\Eloquent\Model;   
use Auth;

class BaseRepository implements EloquentRepositoryInterface 
{     
    /**      
     * @var Model      
     */     
     protected $model;       

    /**      
     * BaseRepository constructor.      
     *      
     * @param Model $model      
     */     
    public function __construct(Model $model)     
    {         
        $this->model = $model;
    }
 
    /**
    * @param array $attributes
    *
    * @return Model
    */
    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }
 
    /**
    * @param $id
    * @return Model
    */
    public function find($id): ?Model
    {
        return $this->model->find($id);
    }


    public function findByColumnSingle($column,$value)
    {
        $object = $this->model->where($column,$value)->first();
        return $object;
    }

    public function findByColumnMultiple($column,$value)
    {
        $object = $this->model->where($column,$value)->get();
        return $object;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function allActive()
    {
        $object = $this->model->where('active',1)->get();
        return $object;
    }

    public function allDesc()
    {
        return $this->model->all()->sortByDesc('id');
    }

    public function whereIn($column,array $values)
    {
        return $this->model->whereIn($column,$values)->get();
    }

   public function delete($id)
   {
        $object = $this->model->find($id);
        if(isset($object->deleted_by))
        {
            $object->deleted_by = Auth::user()->id;
            $object->save();
        }
        $object->delete();

        return $object;
   }

   public function whereMultiple(array $attributes)
   {
        return $this->model->where($attributes)->get();
   }

   public function like($column,$value)
   {
        return $this->model->where($column, 'like', '%'.$value.'%')->get();
   }

   public function where(array $attributes)
   {
    return $this->model->where($attributes)->get();
   }
}
<?php
namespace App\Repository\Eloquent;

use App\Models\CurAccountCategory;
use App\Repository\CurAccountCategoryRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class CurAccountCategoryRepository extends BaseRepository implements CurAccountCategoryRepositoryInterface
{

   public function __construct(CurAccountCategory $model)
   {
       parent::__construct($model);
   }

   public function update(array $attributes,$id)
   {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
   }

}
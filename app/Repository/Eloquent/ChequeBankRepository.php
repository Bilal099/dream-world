<?php
namespace App\Repository\Eloquent;

use App\Models\ChequeBank;
use App\Repository\ChequeBankRepositoryInterface;
use Illuminate\Support\Collection;

class ChequeBankRepository extends BaseRepository implements ChequeBankRepositoryInterface
{
    public function __construct(ChequeBank $model)
    {
        parent::__construct($model);
    }

    public function update(array $attributes,$id)
    {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
    }
}
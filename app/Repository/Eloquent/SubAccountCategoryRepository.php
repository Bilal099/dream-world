<?php
namespace App\Repository\Eloquent;

use App\Models\SubAccountCategory;
use App\Repository\SubAccountCategoryRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class SubAccountCategoryRepository extends BaseRepository implements SubAccountCategoryRepositoryInterface
{

   public function __construct(SubAccountCategory $model)
   {
       parent::__construct($model);
   }

   public function update(array $attributes,$id)
   {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
   }

}
<?php
namespace App\Repository\Eloquent;

use App\Models\Setting;
use App\Repository\SettingsRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class SettingsRepository extends BaseRepository implements SettingsRepositoryInterface
{
    public function __construct(Setting $model)
    {
       parent::__construct($model);
    }

   public function update($attributes)
    {
        $data = $this->model->find($attributes['id']);
        $data->slug         = $attributes['slug'];
        $data->save();
        return $data;
    }

    public function delete($id)
    {
        $data = $this->model->find($id);
        $data->delete();
    }

    public function findSettingDetailsBySettingId($id)
    {
        $setting = $this->model->find($id);
        $settingdetail = $setting->settingdetail;
        return $settingdetail;

    }

    public function findSettingDetailsByCaptchaSettingId($id)
    {
        $captchasetting = $this->model->find($id);
        $settingdetail = $captchasetting->settingdetail;
        // dd($settingdetail);
        return $settingdetail;

        // return [$settingdetail, $setting];
    }

    

}

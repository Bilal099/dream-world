<?php
namespace App\Repository\Eloquent;

use App\Models\SettingDetail;
use App\Repository\SettingDetailRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class SettingDetailRepository extends BaseRepository implements SettingDetailRepositoryInterface
{
    public function __construct(SettingDetail $model)
    {
       parent::__construct($model);
    }

   public function update($attributes,$id)
    {
        // dd($attributes);
        // foreach ($settingDetailattribute as $key => $value) 
        // {
            // $data = $this->model::where([['key','=',$key],['setting_id','=',$id]])->first();
            // $data->value    = $value;
            // $data->save();    
        // }
        $data = $this->model::where([['key','=',$attributes['key']],['setting_id','=',$id]])->first();
        $data->value    = $attributes['value'];
        $data->save();    
        return $data;
    }

    public function delete($id)
    {
        $data = $this->model->find($id);
        $data->delete();
    }
}

<?php
namespace App\Repository\Eloquent;

use App\Models\Director;
use App\Repository\DirectorRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class DirectorRepository extends BaseRepository implements DirectorRepositoryInterface
{

   public function __construct(Director $model)
   {
       parent::__construct($model);
   }

   public function update(array $attributes,$id)
   {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
   }

}
<?php
namespace App\Repository\Eloquent;

use App\Models\User;
use App\Repository\UserRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
   public function __construct(User $model)
   {
       parent::__construct($model);
   }

   /**
    * @return Collection
    */
   

   public function changePassword($id,$password)
   {
        $object = $this->model->find($id);
        $object->password = $password;
        $object->save();
        return $object;
   }

   public function register(array $attributes)
   {
       $object               = new $this->model;
       $object->name         = $attributes['name'];
       $object->email        = $attributes['email']; 
       $object->password     = $attributes['password'];
       $object->save();
       return $object;
   }
}
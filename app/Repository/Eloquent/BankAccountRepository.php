<?php
namespace App\Repository\Eloquent;

use App\Models\BankAccount;
use App\Repository\BankAccountRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class BankAccountRepository extends BaseRepository implements BankAccountRepositoryInterface
{

   public function __construct(BankAccount $model)
   {
       parent::__construct($model);
   }

   public function update(array $attributes,$id)
   {
        $object = $this->model->where('id',$id)->update($attributes);
        return $object;
   }

}
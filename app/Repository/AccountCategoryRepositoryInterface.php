<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface AccountCategoryRepositoryInterface
{

    public function update(array $attributes,$id);
}
<?php
namespace App\Repository;

use App\Models\SettingDetail;
use Illuminate\Support\Collection;

interface SettingDetailRepositoryInterface
{

    public function update($attributes,$id);

    public function delete($id);

}
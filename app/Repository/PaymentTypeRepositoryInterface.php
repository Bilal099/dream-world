<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface PaymentTypeRepositoryInterface
{

    public function update(array $attributes,$id);
}
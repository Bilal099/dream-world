<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface UserRepositoryInterface
{

   public function changePassword($id,$password);

   public function register(array $attributes);
}
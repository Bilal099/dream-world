<?php
namespace App\Repository;

use App\Models\Admin;
use Illuminate\Support\Collection;

interface AdminRepositoryInterface
{

   public function changePassword($id,$password);

   public function register(array $attributes);
}
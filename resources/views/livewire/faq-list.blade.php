

<div class="col-12 mt-2" id="accordion">
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    @if (session()->has('langaugeError'))
        <div class="alert alert-danger">
            {{ session('langaugeError') }}
        </div>
    @endif
    @foreach ($data as $key => $item)
        <div class="card card-primary card-outline">
            <a class="d-block w-100" data-toggle="collapse" href="#{{'collapse'.@$item->id}}">
                <div class="card-header">
                    <h4 class="card-title w-100 white-text">
                        {{@$item->question}}
                    </h4>
                </div>
            </a>
            <div id="{{'collapse'.@$item->id}}" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    {{@$item->answer}}
                </div>
                <div class="m-2" style="float: right">
                    <a href="{{ route('faq.edit',$item->id) }}" class="btn btn-primary">Edit</a>
                    <button type="button" wire:click.prevent="delete({{$item->id}})" class="btn btn-danger delete-btn">Delete</button>
                </div>
            </div>
        </div>
    @endforeach
</div>
@extends('website.layouts.app')
{{-- @php
    $menu     = array();
    $menu_url = array();
    foreach ($menuList as $item) 
    {
        $menu[$item->slug]     = $item->name;
        $menu_url[$item->slug] = $item->link;
    }
@endphp --}}
@section('content')

    <h1>Contact Us</h1>

    {{-- <form id="contact_form" method="POST" data-netlify="true"> --}}
        <div class="form-group">
            <label for="">Name</label>
            <input type="text" class="form-control" id="name" name="name">
          </div>
        <div class="form-group">
          <label for="">Email</label>
          <input type="email" class="form-control" id="email" name="email">
        </div>
        
        
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Message</label>
          <textarea class="form-control" id="message" rows="3" name="message"></textarea>
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-lg btn-primary contact_form">Send</button>
        </div>
      {{-- </form> --}}

      
@endsection

@push('custom-script')
    <script>
      $('.contact_form').click(function (e) { 
        e.preventDefault();
        let name      = $('#name').val();
        let email     = $('#email').val();
        let message   = $('#message').val();
        $.ajax({
          type: "post",
          url: "{{route('AjaxCallForContactUs')}}",
          data: {
            _token: "{{ csrf_token() }}",
            name    : name,
            email   : email,
            message : message,
          },
          success: function (response) {
              console.log(response);
          }
        });
      });
    </script>
@endpush
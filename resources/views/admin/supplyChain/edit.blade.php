@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Update Supply Chain</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <form action="{{route('supplyChain.update',$data->id)}}" method="post" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Date</label>
                                            <input type="date" name="date" id="temp_date" class="form-control" value="{{@$data->date}}" readonly>
                                        </div>
        
                                        <div class="form-group col-md-6">
                                            <label>Cheque Name</label>
                                            <input type="text" name="chq_name" class="form-control" value="{{@$data->vendor->name}}" readonly >
                                            <input type="hidden" name="vendor_id" value="{{@$data->vendor->id}}">
                                        </div>
        
                                        <div class="form-group col-md-6">
                                            <label>NTN/CNIC Number</label>
                                            <input  maxlength="15" type="text" name="cnic" class="form-control" value="{{old('cnic',@$data->cnic_number)}}" readonly>
                                        </div>
        
                                        <div class="form-group col-md-6">
                                            <label>GL IT CODE  (A/Cs)</label>
                                            <input type="text" name="it_code" id="it_code" class="form-control" value="{{@$data->item_code}}" readonly>  
                                        </div>
        
                                        <div class="form-group col-md-6">
                                            <label>Nature Of Work</label>
                                            <input type="text" name="nature_of_work" id="nature_of_work" class="form-control" value="{{old('nature_of_work',@$data->work_of_nature)}}">  
                                        </div>
        
                                        <div class="form-group col-md-6">
                                            <label>Purchaser Person</label>
                                            <input type="text" name="purchaser_person" id="purchaser_person" class="form-control" value="{{old('purchaser_person',@$data->purchaser_person)}}">  
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Total Amount</label>
                                            <input type="text" name="total_amount" id="total_amount" class="form-control" value="{{old('total_amount',@$data->total_amount)}}">  
                                        </div>
        
                                        <div class="form-group col-md-6">
                                            <label>Category</label>
                                            <select name="category" id="category"  class="form-control category">
                                                <option value="" selected>Select Option</option>
                                                @foreach ($category as $obj)
                                                    <option value="{{$obj->id}}" {{old('category',@$data->category_id)==$obj->id? 'selected':''}}>{{$obj->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info">Update</button>
                                <a href="{{route('supplyChain.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>


@endsection


@push('custom-script')

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $("#add_detail").click(function(){  
        $('tbody').append($("#data_row").html());
    });

    $("table").on("click", ".deleteRow", function() {
        $(this).closest("tr").remove();
    });

    $('.chq_name_select').click(function (e) { 
        e.preventDefault();
        $('.chq_name_input').val($(this).html());
    });
   

</script>
@endpush

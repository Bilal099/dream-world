<!-- jQuery -->
<script src="{{asset('backend/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('backend/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)

</script>
<!-- Bootstrap 4 -->
<script src="{{asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
{{-- <!-- ChartJS -->
<script src="{{asset('backend/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('backend/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('backend/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('backend/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script> --}}
<!-- jQuery Knob Chart -->
<script src="{{asset('backend/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('backend/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('backend/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('backend/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('backend/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('backend/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('backend/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('backend/dist/js/demo.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{-- <script src="{{asset('backend/dist/js/pages/dashboard.js')}}"></script> --}}

<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

{{-- <script src="{{asset('backend/plugins/select2/js/select2.min.js')}}"></script> --}}
<script src="{{asset('backend/plugins/select2/js/select2.full.min.js')}}"></script>

<script src="{{asset('backend/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"
    integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g=="
    crossorigin="anonymous"></script> --}}

<script src="{{asset('backend/plugins/jquery-validation/jquery.validate.js')}}"></script>
<script>

    $(document).ready(function () {
        $(".data-table").DataTable();
        $('.select2').select2();
        $(".alert-dismissible").fadeOut(5000);
    });

    function ErrorMsg(msg,container) {
      $(container).html('<ul style="list-style:none" class="pl-0"></ul>');
      $(container).css('display','block');
      $.each( msg, function( key, value ) {
         $(container).find("ul").append('<li>'+value+'</li>');
      });
    }
    //Show Change Password Modal
    $(document).on('click', '#btnChangePassword', function () {
        $('#newpassword_confirmation').val('');
        $('#newpassword').val('');
        $('#oldpassword').val('');
        $('#changePassError').css('display', 'none');
        $('#changePasswordModal').modal('show');
    });

    //Change Password Ajax Request  
    $('#changePassword').submit(function (e) {
        e.preventDefault();
		// console.log('a',new FormData(this));

		var oldpassword = $('#oldpassword').val();	
		var newpassword = $('#newpassword').val();
		var newpassword_confirmation = $('#newpassword_confirmation').val();
        $.ajax({
            url: "{{route('admin.changePassword')}}",
            type: "POST",
            // data: new FormData(this),
            data: {
				_token: "{{ csrf_token() }}",
				oldpassword: oldpassword,
				newpassword: newpassword,
				newpassword_confirmation: newpassword_confirmation,
			},
			// contentType: false,
            // cache: false,
            // processData: false,
            // dataType: 'JSON',
            beforeSend: function () {
                $('#btnsubmitPassChange').prop('disabled', true);
            },
            success: function (data) {

				console.log('data',data);
                if ($.isEmptyObject(data.error)) {
                    if (data.status) {
                        $('#changePasswordModal').modal('hide');
                        toastr.options = {
                            "closeButton": true
                        }
                        toastr["success"]("Password Changed Successfully")
                    }
                } else {
                    changePasswordErrorMsg(data.error, "#changePassError");
                }
            },
            error: function (jhxr, status, err) {
                console.log(jhxr);
            },
            complete: function () {
                $('#btnsubmitPassChange').prop('disabled', false);
            }
        });
    })

</script>


@stack('custom-script')

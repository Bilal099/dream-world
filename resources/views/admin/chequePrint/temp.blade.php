<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<style>
		@page { 
            font-size: 14px;
			margin-right: 5px;
			/* margin: 5px; */
			/* margin: 0%; */
			/* size: 5cm 20cm landscape;  */
		}
		table, td{
			border: 1px solid #000;
		}
	</style>
</head>
{{-- <body style="background-image: url('{{asset('backend/images/temp_cheque.jpeg')}}')"> --}}
<body>
	<table style="width: 100%;">
		<tr>
			<td colspan="2"  style="height: 55px"></td>
		</tr>
		<tr style="">
			<td style="width: 75%" ></td>
			<td style="width: 25%;padding-left:80.5px;" >{{$date}}</td>
		</tr>
		<tr>
			<td colspan="2" style="padding-left: 30%;padding-top: 18px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lorem Ipsum is simply dummy text of the printing and type setting industry </td>
		</tr>
		<tr>
			<td style="width: 85%;padding-left:40%;line-height: 2;" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{amountInWords((float)321456987)}} Only</td>
			<td style="width: 15%;padding-left:75px;padding-top: 15px" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{number_format(321456987)}}</td>
		</tr>
	</table>
</body>
</html>
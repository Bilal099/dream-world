@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Meta Data / SEO</h3>
                        </div>
                        @include('errors.messages')

                        @php
                            $fields = [
                                'url',
                                'title',
                                'keywords',
                                'description',
                                'image',
                                'logo',
                                'favicon',
                            ];
                        @endphp
                        <form id="quickForm" action="{{route('metaTags.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{-- @if (isset($data->id))
                                <input type="hidden" name="id" value="{{ @$data->id }}">
                            @endif --}}
                            
                            <div class="card-body row">
                                @foreach ($fields as $item)
                                    @if (isset($data))
                                        <input type="hidden" name="id[{{$item}}]" value="{{ @$data[$item."_id"] }}">
                                    @endif
                                    <div class="form-group col-md-12">
                                        <label for="{{$item}}">{{ucfirst($item)}}</label>
                                        @if ($item == 'description')
                                            <textarea name="tags[{{$item}}]" class="form-control" id="" cols="30" rows="10">{{(old('tags.'.$item)!=null)? (old('tags.'.$item)):(@$data[$item])}}</textarea>
                                        @elseif ($item == 'image' || $item == 'logo' || $item == 'favicon')
                                            <input type="file" name="tags[{{$item}}]" class="form-control" accept="image" id="{{$item}}" onchange="return CheckDimension(this.id)">
                                            @if (isset($data[$item]))
                                                <br>
                                                <img src="{{$data[$item]}}" alt="" width="auto" >
                                            @endif
                                        @else
                                            <input type="text" name="tags[{{$item}}]" class="form-control" value="{{(old('tags.'.$item)!=null)? (old('tags.'.$item)):(@$data[$item])}}" id="{{$item}}">
                                        @endif
                                        @error('tags.'.$item)
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    @if ($item == 'image')
                                        @error('tags.'.$item)
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    @endif
                                @endforeach
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" id="submit_btn">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('custom-script')

<script type="text/javascript">
    function CheckDimension(id) {
        
        if(id == 'favicon')
        {
            //Get reference of File.
            var fileUpload = document.getElementById(id);
            //Check whether the file is valid Image.
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {
                //Check whether HTML5 is supported.
                if (typeof (fileUpload.files) != "undefined") {

                    //Initiate the FileReader object.
                    var reader = new FileReader();
                    //Read the contents of Image File.
                    reader.readAsDataURL(fileUpload.files[0]);
                    reader.onload = function (e) {
                        //Initiate the JavaScript Image object.
                        var image = new Image();
        
                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;
                            
                        //Validate the File Height and Width.
                        image.onload = function () {
                            var height = this.height;
                            var width = this.width;
                            if (height > 16 || width > 16) {
        
                            //show width and height to user
                                // document.getElementById("width").innerHTML=width;
                                // document.getElementById("height").innerHTML=height;
                                console.log('width',width);
                                console.log('height',height);

                                alert("Height and Width must not exceed 16px.");
                                fileUpload.value = null;
                                return false;
                            }
                            alert("Uploaded image has valid Height and Width.");
                            return true;
                        };
        
                    }
                } else {
                    alert("This browser does not support HTML5.");
                    return false;
                }
            } else {
                alert("Please select a valid Image file.");
                return false;
            }
        }
        
    }
</script>
<script> 
$('#submit_btn').click(function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'Do you want to save the changes?',
            showCancelButton: true,
            confirmButtonText: `Save`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                $('#quickForm').submit();
            } else{
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })
</script>
@endpush
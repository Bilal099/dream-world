@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Accountant Category</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <form action="{{route('accountCategory.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if (isset($data->id))
                                    <input type="hidden" name="id" value="{{ @$data->id }}">
                                @endif
                                <div class="card-body row">
                                    <div class="form-group col-md-12">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" value="{{(old('name')!=null)? (old('name')):(@$data->name)}}" >
                                        @error('name')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Code</label>
                                        <input type="text" name="code" class="form-control" value="{{(old('code')!=null)? (old('code')):(@$data->code)}}" >
                                        @error('code')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label>Select Department</label>
                                            <select class="form-control select2" name="department_id" data-placeholder="Select a Department" style="width: 100%;">
                                                {{-- <option disabled selected>Select Option</option> --}}
                                                @foreach ($department as $item)
                                                    <option value="{{$item->id}}" {{old('department_id',@$data->department_id)==$item->id ? 'selected':'' }}>{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('parent_id')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                            @enderror  
    
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{route('accountCategory.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>

@endsection


@push('custom-script')

<script>
    // $(function () {
    //     $('.select2').select2();
    // });

    $(document).ready(function () {
        $('.select2').select2();
    });

   

</script>
@endpush

@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Bank Account</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <form action="{{route('bankAccount.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if (isset($data->id))
                                    <input type="hidden" name="id" value="{{ @$data->id }}">
                                @endif
                                <div class="card-body row">
                                    <div class="form-group col-md-12">
                                        <label>Name</label>
                                        <select name="bank_id" class="form-control" id="">
                                            <option value="" selected >Select Bank</option>
                                            @foreach ($banks as $item)
                                                <option value="{{$item->id}}" {{old('bank_id',@$data->bank_id)==$item->id? 'selected':''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('bank_id')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Branch Code</label>
                                        <input type="text" name="branch_code" class="form-control" value="{{(old('branch_code')!=null)? (old('branch_code')):(@$data->branch_code)}}" >
                                        @error('branch_code')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    
                                    <div class="form-group col-md-12">
                                        <label>Account Number</label>
                                        <input type="text" name="account_no" class="form-control" value="{{(old('account_no')!=null)? (old('account_no')):(@$data->account_no)}}" >
                                        @error('account_no')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{route('bankAccount.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>

@endsection


@push('custom-script')

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $("#add_detail").click(function(){  
        $('tbody').append($("#data_row").html());
        // $('.export-cargo-detail-date').val($('#of_date').val());
    });

    $("table").on("click", ".deleteRow", function() {
        $(this).closest("tr").remove();
    });

    $('.chq_name_select').click(function (e) { 
        e.preventDefault();
        // console.log($(this).html());
        $('.chq_name_input').val($(this).html());
    });
   

</script>
@endpush

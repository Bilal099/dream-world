@extends('admin.layouts.app')

@push('custom-css')

<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View Bank Accounts</h3>
                            @can('bank-create')
                            <a class="btn btn-success btn-sm float-right" href="{{route('bankAccount.create')}}">
                                Add Record
                            </a>
                            @endcan
                        </div>
                        <div class="card-body">
                            @include('errors.messages')
                            <table class="table table-bordered table-striped" id="data-table">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Bank Name</th>
                                        <th>Branch Code</th>
                                        <th>Account Number</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    @endphp
                                    @foreach ($data as $key => $item)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{@$item->bank->name}}</td>
                                        <td>{{@$item->branch_code}}</td>
                                        <td>{{@$item->account_no}}</td>
                                        <td class="center">
                                            @can('bank-account-edit')
                                            <a class="btn btn-primary btn-sm ml-1"
                                                href="{{route('bankAccount.edit',$item->id)}}" data-toggle="tooltip"
                                                data-placement="top" title="edit">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            @endcan

                                            @can('bank-account-delete')
                                            <form action="{{route('bankAccount.destroy',$item->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm ml-1 btn-delete"
                                                    data-id="{{$item->id}}" data-toggle="tooltip" data-placement="top"
                                                    title="Delete">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                            @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

</div>



@endsection


@push('custom-script')

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let id = _this.data('id');
        let form = _this.closest('form');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                form.submit();
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush


@extends('admin.layouts.app')
@push('custom-css')

@endpush
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
                @if (Session::has('message'))
                <div class="alert alert-success mt-2" role="alert">
                     <span> <strong>Message!</strong> {{Session::get('message')}}</span>
                </div>
                @endif
                <div class="card mt-3">
                    <div class="card-header">
                      <h3 class="card-title">View All Users</h3>
                    <a class="btn btn-success btn-sm float-right" href="{{route('users.create')}}"><i class="fa fa-plus"></i></a>
                    
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-striped" id="example1">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    {{-- @role('superadmin')
                                    <th>Role</th>
                                    @endrole --}}
                                    <th>Active</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $key=> $item)
                                    <tr>
                                    <td>{{++$key}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->email}}</td>
                                        {{-- @role('superadmin')
                                        <td>{{$item->getRoleNames()[0]}}</td>
                                        @endrole --}}
                                        <td>{{$item->active == 1?'active':'deactive'}}</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm btnEdit" data-value="{{$item->id}}"><i class="fa fa-edit"></i></button>
                                            <a href="{{route("auth.user.permissions",$item->id)}}" title="permissions" class="btn btn-sm btn-secondary"><i class="fa fa-lock"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </section>
</div>
@include('admin.partials._modal')
@endsection

@push('custom-script')

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $(document).on('click','.btnEdit',function(){
        idVal = $(this).attr('data-value');
        $('#frontPagesModal').modal('show').find('.modal-content').html(`<div class="modal-body">
                <div class="overlay text-center"><i class="fas fa-2x fa-sync-alt fa-spin text-light"></i></div>
            </div>`);
            $.ajax({
                method:'get',
                url:'/admin/users/'+idVal+'/edit',
                dataType: 'html',
                success:function(res){
                    $('#frontPagesModal .modal-content').html(res);
                }
        });
    });
        $(document).on('submit','#frontPagesModal form',function(e){
            url = $(this).attr('action');
            formData = new FormData(this);
            e.preventDefault();
            $.ajax({
                url: url,
                type: "POST",
                data:  formData,
                contentType: false,
                cache: false,
                processData:false,
                dataType:'JSON',
                success:function(res){
                    if($.isEmptyObject(res.error)){
                    if(res.status)
                    {
                        window.location.reload();
                    }
                    }else{
                    ErrorMsg(res.error,"#frontPagesModal #modelError");
                    }
                },
                error:function(jhxr,status,err)
                {
                    console.log(jhxr);
                },
                complete:function()
                {

                }
            });
        });
</script>
@endpush
@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Cheque</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <form action="{{route('cheque.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if (isset($data->id))
                                    <input type="hidden" name="id" value="{{ @$data->id }}">
                                @endif
                                <div class="card-body row">
                                    <div class="form-group col-md-12">
                                        <label>Bank Name</label>
                                        <input type="text" name="bank_name" class="form-control" value="{{(old('bank_name')!=null)? (old('bank_name')):(@$data->bank_name)}}" >
                                        @error('bank_name')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Blade name</label>
                                        <input type="text" name="blade_name" class="form-control" value="{{(old('blade_name')!=null)? (old('blade_name')):(@$data->blade_name)}}" >
                                        @error('blade_name')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="form-check ">
                                        <input type="checkbox" class="form-check-input" name="default" id="default"  {{old('default')=="on"||@$data->default==1? 'checked':''}}>  Set as Default.
                                        @error('default')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror 
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{route('cheque.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>

@endsection


@push('custom-script')
<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $("#add_detail").click(function(){  
        $('tbody').append($("#data_row").html());
        // $('.export-cargo-detail-date').val($('#of_date').val());
    });

    $("table").on("click", ".deleteRow", function() {
        $(this).closest("tr").remove();
    });

    $('.chq_name_select').click(function (e) { 
        e.preventDefault();
        // console.log($(this).html());
        $('.chq_name_input').val($(this).html());
    });
   

</script>
@endpush

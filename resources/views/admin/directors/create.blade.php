@extends('admin.layouts.app')

@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" /> --}}
<style>
    /* .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    } */

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Director</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <form action="{{route('director.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if (isset($data->id))
                                    <input type="hidden" name="id" value="{{ @$data->id }}">
                                @endif
                                <div class="card-body row">
                                    <div class="form-group col-md-12">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" value="{{(old('name')!=null)? (old('name')):(@$data->name)}}" >
                                        @error('name')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{route('director.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>
<noscript type="text/html" id="data_row">
    
    <tr>
        <td>
            <input type="date" name="date[]" class="form-control export-cargo-detail-date" placeholder="" id="">
        </td>
        <td>
            <input type="text" name="chq_name[]" class="" value="" placeholder="" id="">
        </td>
        <td>
            <input type="text" name="cnic[]" class="" value="" placeholder="" id="">
        </td>
        <td>
            <input type="text" name="gt_item_code[]" class="" value="" placeholder="" id="">
        </td>
        <td>
            <input type="text" name="nature_of_work[]" class="" value="" placeholder="" id="">
        </td>
        <td>
            <input type="text" name="purchaser_person[]" class="" value="" placeholder="" id="">
        </td>
        <td>
            <input type="text" name="total_amount[]" class="" value="" placeholder="" id="">
        </td>
        <td>
            <input type="text" name="category[]" class="" value="" placeholder="" id="">
        </td>
        <td>
            <a class="btn btn-danger deleteRow"> <i class="fa fa-trash"></i></a> 
        </td> 
    </tr>
</noscript>


@endsection


@push('custom-script')
<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"></script>

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $("#add_detail").click(function(){  
        $('tbody').append($("#data_row").html());
        // $('.export-cargo-detail-date').val($('#of_date').val());
    });

    $("table").on("click", ".deleteRow", function() {
        $(this).closest("tr").remove();
    });

    $('.chq_name_select').click(function (e) { 
        e.preventDefault();
        // console.log($(this).html());
        $('.chq_name_input').val($(this).html());
    });
   

</script>
@endpush

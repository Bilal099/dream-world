@extends('admin.layouts.app')

@push('custom-css')

<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .total-amount {
        background-color: rgb(253, 93, 93) !important;
    }

    .remaining-amount{
        background-color: rgb(93, 253, 101) !important;
    }

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View Supply Chain</h3>
                            <a class="btn btn-success btn-sm float-right" href="{{route('supplyChain.create')}}">
                                Add Record
                                {{-- <i class="fa fa-plus"></i> --}}
                            </a>

                        </div>
                        <form action="" method="GET" id="supply_chain_form">
                            {{-- @csrf --}}
                            <div class="card-body">
                                @include('errors.messages')
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped " id="data-table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Date</th>
                                                <th>Cheque Name</th>
                                                <th>Cnic Number</th>
                                                <th>Item Code</th>
                                                <th>Work Of Nature</th>
                                                <th>Purchaser Person</th>
                                                <th>Bill Amount</th>
                                                <th>Category</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach ($data_not_in_process as $key => $item)
                                            {{-- @if ($item->director_id == null || $item->accountant_id != null ) --}}
                                                {{-- @if(in_array($item->id,$is_complete) || !in_array($item->id,$in_process)) --}}
                                            @if(!in_array($item->id,$in_process))
                                            
                                            <tr>
                                                <td>
                                                    <div class="form-group col-md-12">
                                                        <div class="form-check ">
                                                        <input type="checkbox" class="form-check-input" name="active" id="" value="" >  {{$i++}}
                                                        <input type="hidden" name="id[]" value="{{$item->id}}" class="supply_chain_hidden_field" disabled>
                                                        {{-- supply_chain_check_box --}}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{@$item->date}}</td> 
                                                <td>{{@$item->vendor->cheque_name}}</td> 
                                                {{-- <td>{{@$item->cheque_name}}</td>  --}}
                                                <td>{{@$item->cnic_number}}</td> 
                                                <td>{{@$item->item_code}}</td> 
                                                <td>{{@$item->work_of_nature}}</td> 
                                                <td>{{@$item->purchaser_person}}</td> 
                                                <td>
                                                    {{@number_format($item->remaining_amount)}}
                                                </td> 
                                                <td>
                                                    {{-- {{@$item->category_name}} --}}
                                                    {{@$item->category->name}}
                                                </td> 
                                                <td class="">
                                                    @can('supply-chain-view')
                                                        <a class="btn btn-secondary btn-sm view-btn ml-1 mt-1" href="{{route('supplyChain.show',$item->id)}}" data-toggle="tooltip" data-placement="top" title="View">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    @endcan

                                                    @if (@count($item->paymentNotProceed) == 0)
                                                        @can('supply-chain-edit')
                                                            <a class="btn btn-primary btn-sm ml-1 mt-1" href="{{route('supplyChain.edit',$item->id)}}" data-toggle="tooltip" data-placement="top" title="edit">
                                                                <i class="fas fa-edit"></i>
                                                            </a>      
                                                        @endcan
                                                    
                                                        @can('supply-chain-delete')
                                                            <button class="btn btn-danger btn-sm ml-1 mt-1 btn-delete" data-id="{{$item->id}}" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        @endcan
                                                    @endif
                                                    

                                                    
                                                </td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer">
                                {{-- @can('supply-chain-bulk-edit')
                                    <a class="btn btn-md btn-warning" id="bulk_edit">Bulk Edit</a>
                                @endcan --}}

                                @can('supply-chain-daily-sheet')
                                    <a class="btn btn-md btn-danger" id="bulk_pdf">Daily Report</a>
                                @endcan

                                <a class="btn btn-md btn-info" id="bulk_approve">Approve</a>

                            </div>
                        </form>

                    </div>
                </div>
                <!-- Approve by accountant  -->
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View Supply Chain Approve By Accountant</h3>
                        </div>
                        <form action="" method="GET" id="supply_chain_form">
                            {{-- @csrf --}}
                            <div class="card-body">
                                @include('errors.messages')
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" id="data-table1">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Date</th>
                                                <th>Cheque Name</th>
                                                <th>Cnic Number</th>
                                                <th>Item Code</th>
                                                <th>Work Of Nature</th>
                                                <th>Purchaser Person</th>
                                                <th>Total Amount</th>
                                                <th>Remaining Amount</th>
                                                <th>Category</th>
                                                {{-- <th>Mode of Payment</th>
                                                <th>Approved By</th> --}}
                                                <th>Paid Amount</th>
                                                {{-- <th>Last Approve Amount</th> --}}
                                                {{-- <th>Action</th> --}}

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach ($data_complete as $key => $item)
                                            {{-- @dd($item->accountant_id) --}}
                                            {{-- @if ($item->accountant_id != null) --}}
                                            <tr>
                                                <td>
                                                    {{-- <div class="form-group col-md-12">
                                                        <div class="form-check ">
                                                        <input type="checkbox" class="form-check-input" name="active" id="" value="" >  --}}
                                                        {{$i++}}
                                                        {{-- <input type="hidden" name="id[]" value="{{$item->id}}" class="supply_chain_hidden_field" disabled>
                                                        </div> --}}
                                                    </div>
                                                </td>
                                                
                                                <td>{{@$item->date}}</td> 
                                                <td>{{@$item->vendor->cheque_name}}</td> 
                                                {{-- <td>{{@$item->cheque_name}}</td>  --}}
                                                <td>{{@$item->cnic_number}}</td> 
                                                <td>{{@$item->item_code}}</td> 
                                                <td>{{@$item->work_of_nature}}</td> 
                                                <td>{{@$item->purchaser_person}}</td> 
                                                <td>{{@number_format($item->total_amount)}}</td> 
                                                <td>{{@number_format($item->remaining_amount)}}</td> 
                                                {{-- <td>{{@number_format($item->total_amount - $item->paid_amount)}}</td>  --}}
                                                {{-- <td>{{@number_format($item->total_amount - ($item->amount_paid+$item->remaining_amount))}}</td>  --}}
                                                <td>{{@$item->category->name}}</td>
                                                {{-- <td>{{@$item->payment->name}}</td>
                                                <td>{{@$item->director->name}}</td> --}}
                                                {{-- <td>{{@$item->approved_amount_by_director}}</td> --}}
                                                {{-- <td>{{@number_format($item->amount_paid)}}</td> --}}
                                                <td>{{@number_format($item->total_amount - $item->remaining_amount)}}</td> 
                                                {{-- <td class="center">
                                                    @can('supply-chain-view')
                                                        <a class="btn btn-secondary btn-sm view-btn" href="{{route('supplyChain.show',$item->id)}}" data-toggle="tooltip" data-placement="top" title="View">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    @endcan

                                                    @can('supply-chain-edit')
                                                        <a class="btn btn-primary btn-sm ml-1" href="{{route('supplyChain.edit',$item->id)}}" data-toggle="tooltip" data-placement="top" title="edit">
                                                            <i class="fas fa-edit"></i>
                                                        </a>      
                                                    @endcan
                                                
                                                    @can('supply-chain-delete')
                                                        <button class="btn btn-danger btn-sm ml-1 btn-delete" data-id="{{$item->id}}" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    @endcan
                                                </td> --}}
                                            </tr>
                                            {{-- @endif --}}
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{-- <div class="card-footer">
                                @can('supply-chain-bulk-edit')
                                    <a class="btn btn-md btn-warning" id="bulk_edit">Bulk Edit</a>
                                @endcan

                                @can('supply-chain-daily-sheet')
                                    <a class="btn btn-md btn-danger" id="bulk_pdf">Daily Report</a>
                                @endcan

                                <a class="btn btn-md btn-info" id="bulk_approve">Approve</a>

                            </div> --}}
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

</div>



@endsection


@push('custom-script')


<script>
    $(function () {
        $("#data-table").DataTable();
        var table1 = $("#data-table1").DataTable();
        
        table1.rows().every( function ( rowIdx, tableLoop, rowLoop ) {  
            var cell1 = table1.cell({ row: rowIdx, column: 8 }).node();     
            var cell2 = table1.cell({ row: rowIdx, column: 7 }).node();
            $(cell1).addClass('remaining-amount');        
            $(cell2).addClass('total-amount');
        });
    });

    $('.form-check-input').change(function (e) { 
        e.preventDefault();
        let _this = $(this);
        if (_this.is(':checked')) 
        {
            _this.closest('div').find('.supply_chain_hidden_field').prop('disabled',false);
        } 
        else {
            _this.closest('div').find('.supply_chain_hidden_field').prop('disabled',true);
        }
    });

    $('#bulk_edit').click(function(e){
        e.preventDefault();
        let ids = Array();
        let check_count = 0;
        $.each($('.form-check-input'), function (indexInArray, valueOfElement) { 
            let _this = $(this);
            if (_this.is(':checked')) 
            {
                ids[check_count] = _this.closest('div').find('.supply_chain_hidden_field').val();
                check_count++;
            }
        });
        if (check_count > 0) 
        {
            let json_request = JSON.stringify(ids);
            let url = 'supplyChain/bulkEdit/'+btoa(json_request);
            $('#supply_chain_form').attr('action',url);
            $('#supply_chain_form').submit();
        } 
        else {
            alert('Select at least one record');    
        }
    });

    $('#bulk_approve').click(function(e){
        e.preventDefault();
        let ids = Array();
        let check_count = 0;
        $.each($('.form-check-input'), function (indexInArray, valueOfElement) { 
            let _this = $(this);
            if ($(this).is(':checked')) 
            {
                ids[check_count] = _this.closest('div').find('.supply_chain_hidden_field').val();
                check_count++;
            }
        });
        if (check_count > 0) 
        {
            let json_request = JSON.stringify(ids);
            let url = 'supplyChain/approve/'+btoa(json_request);
            $('#supply_chain_form').attr('action',url);
            $('#supply_chain_form').submit();
        } 
        else {
            alert('Select at least one record');    
        }
    });

    $('#bulk_pdf').click(function(e){
        e.preventDefault();
        let ids = Array();
        let check_count = 0;
        $.each($('.form-check-input'), function (indexInArray, valueOfElement) { 
            let _this = $(this);
            if ($(this).is(':checked')) 
            {
                ids[check_count] = _this.closest('div').find('.supply_chain_hidden_field').val();
                check_count++;
            }
        });
        if (check_count > 0) 
        {
            let json_request = JSON.stringify(ids);
            let url = 'supplyChain/bulkPDF/'+btoa(json_request);
            $('#supply_chain_form').attr('action',url);
            $('#supply_chain_form').attr('target', '_blank').submit();
            // $('#supply_chain_form').submit();
        } 
        else {
            alert('Select at least one record');    
        }
    });

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let id = _this.data('id'); 
        // let form = _this.closest('form');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    url: "supplyChain/"+id,
                    type: "DELETE",
                    data: {
                        "id": id,
                        "_token": '{{ csrf_token() }}',
                    },
                    success: function (response) {
                        console.log('response',response['status']);
                        if(response['status'])
                        {
                            $('#success_msg_id').text(response['msg']);
                            $('#success_msg').show();
                            _this.closest('tr').remove();
                        }
                        else{
                            $('#error_msg_id').text(response['msg']);
                            $('#error_msg').show();
                        }
                        $(".alert-dismissible").fadeOut(5000);
                    }
                });
            } 
            else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush

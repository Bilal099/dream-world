@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card card-primary mt-4">
            <div class="card-header">
              <h3 class="card-title">Settings {{ucfirst(@$slug)}}</h3>
            </div>
            @include('errors.messages')

            <form id="quickForm" action="{{route('settings.store')}}" method="post" enctype="multipart/form-data">
              @csrf
              @if (isset($data->id))
              <input type="hidden" name="id" value="{{ @$data->id }}">
              <input type="hidden" name="slug" value="{{ @$data->slug }}">
              @else
              <input type="hidden" name="slug" value="{{ @$slug }}">
              @endif
              <div class="card-body row">

                @foreach ($fields as $item)
                @if (str_contains($item,'PASSWORD'))

                <div class="form-group col-md-6">
                  <label for="value">{{str_replace("_", " ", $item)}}</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                    </div>
                    <input name="setting[{{$item}}]" type="password" value="{{@$settingdetail[$item]}}"
                      class="input form-control" id="password" placeholder="password" required="true"
                      aria-label="password" aria-describedby="basic-addon1" />
                    <div class="input-group-append">
                      <span class="input-group-text" onclick="password_show_hide();">
                        <i class="fas fa-eye" id="show_eye"></i>
                        <i class="fas fa-eye-slash d-none" id="hide_eye"></i>
                      </span>
                    </div>
                  </div>
                  @error('setting.'.$item)
                  <p class="text-danger text-sm">{{$message}}</p>
                  @enderror
                </div>
                @else
                <div class="form-group col-md-6">
                  <label for="value">{{str_replace("_", " ", $item)}}</label>
                  <input type="text" name="setting[{{$item}}]" class="form-control" value="{{@$settingdetail[$item]}}"
                    id="">
                  @error('setting.'.$item)
                  <p class="text-danger text-sm">{{$message}}</p>
                  @enderror
                </div>
                @endif
                @endforeach
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" id="submit_btn">Update</button>
                {{-- <a href="{{route('admin.settings')}}" class="btn btn-default">Back</a> --}}
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@push('custom-script')


<script>
  function password_show_hide() {
    var x = document.getElementById("password");
    var show_eye = document.getElementById("show_eye");
    var hide_eye = document.getElementById("hide_eye");
    hide_eye.classList.remove("d-none");
    if (x.type === "password") {
      x.type = "text";
      show_eye.style.display = "none";
      hide_eye.style.display = "block";
    } else {
      x.type = "password";
      show_eye.style.display = "block";
      hide_eye.style.display = "none";
    }
  }

  $('#submit_btn').click(function (e) {
    e.preventDefault();

    Swal.fire({
      title: 'Do you want to save the changes?',
      showCancelButton: true,
      confirmButtonText: `Save`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        // Swal.fire('Saved!', '', 'success')
        $('#quickForm').submit();
      } else {
        Swal.fire('Changes are not saved', '', 'info')
      }
    })
  })

</script>
@endpush

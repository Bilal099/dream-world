@extends('admin.layouts.app')

@push('custom-css')

  <style>
    .center{
      text-align: center !important;
    }
    .border-top-bottom{
      border-top: 1px solid #adacac;
      border-bottom: 1px solid #adacac;
      background-color: #adacac;
    }
  </style>
@endpush

@section('content')
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card card-primary mt-4">
            <div class="card-header">
              <h3 class="card-title">Add Module</h3>
            </div>
            @include('errors.messages')

            <form id="quickForm" action="{{route('module.store')}}" method="post" enctype="multipart/form-data">
              @csrf
              @if (isset($data->id))
                <input type="hidden" name="id" value="{{ @$data->id }}">
              @endif
              <div class="card-body row">
                <div class="form-group col-md-12">
                    <label for="name">Module Name</label>
                    <input type="text" name="name" class="form-control" value="{{old('name',@$data->name)}}" id="">
                    @error('name')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror
                </div>

                <div class="form-group col-md-12">
                  <h3 class="border-top-bottom center">Repository</h3>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-check ">
                    <input type="checkbox" class="form-check-input" name="repository_check" id="repository_check" > Module Repository
                    @error('repository_check')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror 
                    </div>
                </div>

                <div class="form-group col-md-12">
                  <h3 class="border-top-bottom center">Model</h3>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-check ">
                    <input type="checkbox" class="form-check-input" name="model_migration_check" id="model_migration_check" > Model With Migration
                    @error('model_migration_check')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror 
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-check ">
                    <input type="checkbox" class="form-check-input" name="model_check" id="model_check" > Model
                    @error('model_check')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror 
                    </div>
                </div>
                
                <div class="form-group col-md-12">
                  <h3 class="border-top-bottom center">Controller</h3>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-check ">
                    <input type="checkbox" class="form-check-input" name="resource_controller_check" id="resource_controller_check" > Resource Controller
                    @error('resource_controller_check')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror 
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-check ">
                    <input type="checkbox" class="form-check-input" name="controller_check" id="controller_check" > Controller
                    @error('controller_check')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror 
                    </div>
                </div>
                
                <div class="form-group col-md-12">
                  <h3 class="border-top-bottom center">View files</h3>
                </div>

                <div class="form-group col-md-3">
                    <div class="form-check ">
                    <input type="checkbox" class="form-check-input" name="index_blade_check" id="index_blade_check" > Index Blade
                    @error('index_blade_check')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror 
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <div class="form-check ">
                    <input type="checkbox" class="form-check-input" name="create_blade_check" id="create_blade_check" > Create Blade
                    @error('create_blade_check')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror 
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <div class="form-check ">
                    <input type="checkbox" class="form-check-input" name="edit_blade_check" id="edit_blade_check" > Edit Blade
                    @error('edit_blade_check')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror 
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <div class="form-check ">
                    <input type="checkbox" class="form-check-input" name="show_blade_check" id="show_blade_check" > Show Blade
                    @error('show_blade_check')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror 
                    </div>
                </div>

                {{-- <div class="form-group col-md-12">
                    <div class="form-check ">
                    <input type="checkbox" class="form-check-input" name="status" id="status" checked>  Is Active?
                    @error('status')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror 
                    </div>
                </div> --}}
               
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" id="submit_btn"> {{!isset($data)? 'Create':'Update'}}</button>
                {{-- <a href="{{route('module.index')}}" class="btn btn-default">Back</a> --}}
                <a href="{{route('admin.home')}}" class="btn btn-default">Back</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@push('custom-script')


<script>
 
  $('#model_migration_check').click(function () { 
    document.getElementById("model_check").checked = false;
  });
  
  $('#model_check').click(function () { 
    document.getElementById("model_migration_check").checked = false;
  });
  
  $('#resource_controller_check').click(function () { 
    document.getElementById("controller_check").checked = false;
  });
  
  $('#controller_check').click(function () { 
    document.getElementById("resource_controller_check").checked = false;
  });

  $('#submit_btn').click(function (e) {
    e.preventDefault();

    Swal.fire({
      title: 'Do you want to save the changes?',
      showCancelButton: true,
      confirmButtonText: `Save`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        // Swal.fire('Saved!', '', 'success')
        $('#quickForm').submit();
      } else {
        Swal.fire('Changes are not saved', '', 'info')
      }
    })
  })

</script>
@endpush

@extends('admin.layouts.app')

@push('custom-css')

<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Import Cur Accountant Categories</h3>
                        </div>
                        <div class="card-body">
                            @include('errors.messages')

                            <form action="{{ route('curCategory.import') }}" method="POST" enctype="multipart/form-data">

                                @csrf

                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label>Select Department</label>
                                        <select class="form-control select2" name="department_id" id="department_id" data-placeholder="Select a Department" style="width: 100%;">
                                            <option disabled selected>Select Option</option>
                                            @foreach ($department as $item)
                                                <option value="{{$item->id}}" {{old('department_id',@$data->department_id)==$item->id ? 'selected':'' }}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('department_id')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  

                                    </div>
                                </div>

                                {{-- <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label>Select Main Category</label>
                                        <select class="form-control select2" name="account_category_id" id="account_category_id" data-placeholder="Select a main" style="width: 100%;">
                                           @if (isset($main))
                                                @foreach ($main as $item)
                                                    <option value="{{$item->id}}" {{old('account_category_id',@$data->account_category_id)==$item->id ? 'selected':'' }}>{{$item->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @error('account_category_id')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror  

                                    </div>
                                </div> --}}

                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label>Select File</label>
                                        <input type="file" name="file" class="form-control">
                                        @error('file')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror 
                                    </div>
                                </div>
                
                
                                <br>
                
                                <button class="btn btn-success">Import Cur Accountant Categories Data</button>
                
                
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

</div>



@endsection


@push('custom-script')

<script>
   var option;
    $('#department_id').change(function (e) { 
        e.preventDefault();

        let _this = $(this);
        let department_id = _this.val();

        $.ajax({
            type: "POST",
            url: "{{route('getMainCategoryByDepartment')}}",
            data: 
            {
                _token: "{{csrf_token()}}",
                department_id:department_id
            },
            success: function (response) {
                if (response.status) 
                {
                    $("#account_category_id option").remove();
                    let data =  response.data; 
                    option = '';
                    option += '<option value="" selected>Select Vendor</option>';
                    for (var val in data) {
                        option += '<option value="' + data[val].id + '">' + data[val].name + '</option>';
                    }  
                    $('#account_category_id').append(option);
                }
                
            }
        });
    });

</script>
@endpush

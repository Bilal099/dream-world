@extends('admin.layouts.app')

@push('custom-css')

<style>
    /* .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    } */

    .center {
        text-align: center !important;
    }

    /* th{
        width: 50% !important;
    } */

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card  mt-4">
                        @include('errors.messages')

                        <form action="{{route('vouchar.store')}}" method="POST">
                            @csrf
                            @if (isset($data->id))
                                <input type="hidden" name="id" value="{{$data->id}}">
                            @endif
                            <input type="hidden" name="party_id" value="{{@$data->payment->vendor->id}}">
                            <div class="card-header">
                                <h3 class="card-title">Create Vouchar Record</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">    
                                        <table class="table table-striped " >
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>VCH</th>
                                                    <th>Cheque Name</th>
                                                    <th>Item Code</th>
                                                    <th>Debit</th>
                                                    <th>Credit</th>
                                                    <th>Description</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $total_amount = $data->payment->amount_paid;
                                                    if(@$data->payment->vendor->tax_rate != null)
                                                    {
                                                        $tax_amount = ($data->payment->amount_paid/100) * $data->payment->vendor->tax_rate; 
                                                        $total_amount = $total_amount - $tax_amount;
                                                    }
                                                @endphp
                                                <tr>
                                                    <td>{{@$data->vouchar_date}}</td>
                                                    <td>{{@$data->VCH}}</td>
                                                    <td>{{@$data->payment->vendor->cheque_name}}</td>
                                                    <td>{{@$data->payment->vendor->it_code}}</td>
                                                    
                                                    <td>{{number_format(@$total_amount)}}</td>
                                                    <td></td>
                                                    <td>
                                                        <input type="text" name="description[]">
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>{{@$data->vouchar_date}}</td>
                                                    <td>{{@$data->VCH}}</td>
                                                    <td>
                                                        <select class="select2 vendor_id" name="bank_vendor_id" id="bank_vendor_id">
                                                            <option value="">Select Option</option>
                                                            @foreach ($vendors as $item)
                                                            {{-- <option value="{{@$item->id}}" data-it_code="{{@$item->it_code}}">{{@$item->cheque_name}}</option> --}}
                                                            <option value="{{@$item->vendor_id}}" data-it_code="{{@$item->vendor->it_code}}" {{(@$item->default==1)? 'selected':''}}>{{@$item->vendor->name}}</option>
                                                            @endforeach
                                                        </select>    
                                                    </td>
                                                    <td class="it_code">
                                                    </td>
                                                    <td></td>
                                                    <td>{{number_format(@$total_amount)}}</td>
                                                    <td>
                                                        <input type="text" name="description[]">
                                                    </td>
                                                </tr>

                                                @if (@$data->payment->vendor->tax_rate!=null)
                                                    {{-- @php
                                                        if(@$data->payment->vendor->tax_rate != null)
                                                        {
                                                            $tax_amount = ($data->payment->amount_paid/100) * $data->payment->vendor->tax_rate; 
                                                        }
                                                    @endphp --}}
                                                    <tr>
                                                        <th colspan="7" class="center">For Tax</th>
                                                    </tr>
                                                    <tr>
                                                        <td>{{@$data->vouchar_date}}</td>
                                                        <td>{{@$data->VCH}}</td>
                                                        <td>
                                                            <select class="select2 vendor_id" name="payable_tax_id" id="payable_tax_id">
                                                                <option value="">Select Option</option>
                                                                @foreach ($vendors as $item)
                                                                {{-- <option value="{{@$item->id}}" data-it_code="{{@$item->it_code}}">{{@$item->cheque_name}}</option> --}}
                                                                <option value="{{@$item->vendor_id}}" data-it_code="{{@$item->vendor->it_code}}" >{{@$item->vendor->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td class="it_code"></td>
                                                        <td>
                                                            {{number_format(@$tax_amount)}}
                                                        </td>
                                                        <td></td>
                                                        <td>
                                                            <input type="text" name="description[]">
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td>{{@$data->vouchar_date}}</td>
                                                        <td>{{@$data->VCH}}</td>
                                                        <td id="bank_id_tax">
                                                            {{-- <select class="select2 vendor_id" name="vendor_id" id="vendor_id">
                                                                <option value="">Select Option</option>
                                                                @foreach ($vendors as $item)
                                                                <option value="{{@$item->id}}" data-it_code="{{@$item->it_code}}">{{@$item->cheque_name}}</option>
                                                                @endforeach
                                                            </select>     --}}
                                                        </td>
                                                        <td class="it_code"></td>
                                                        <td></td>
                                                        <td>{{number_format(@$tax_amount)}}</td>
                                                        <td>
                                                            <input type="text" name="description[]">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th colspan="7" class="center">For Journal Vouchar</th>
                                                    </tr>
                                                    <tr>
                                                        <td>{{@$data->vouchar_date}}</td>
                                                        <td>JV</td>
                                                        <td>{{@$data->payment->vendor->cheque_name}}</td>
                                                        <td>{{@$data->payment->vendor->it_code}}</td>
                                                        <td>{{number_format(@$tax_amount)}}</td>
                                                        <td></td>
                                                        <td>
                                                            <input type="text" name="description[]">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>{{@$data->vouchar_date}}</td>
                                                        <td>JV</td>
                                                        <td>
                                                            <select class="select2 vendor_id" name="tax_record_id" id="tax_record_id">
                                                                <option value="">Select Option</option>
                                                                @foreach ($vendors as $item)
                                                                {{-- <option value="{{@$item->id}}" data-it_code="{{@$item->it_code}}">{{@$item->cheque_name}}</option> --}}
                                                                <option value="{{@$item->vendor_id}}" data-it_code="{{@$item->vendor->it_code}}" >{{@$item->vendor->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td class="it_code"></td>
                                                        <td></td>
                                                        <td>
                                                            {{number_format(@$tax_amount)}}
                                                        </td>
                                                        <td>
                                                            <input type="text" name="description[]">
                                                        </td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Narration</label>
                                            <input type="text" name="narration" id="narration" class="form-control" required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <input type="hidden" name="party_amount" value="{{$total_amount}}">
                                @if (isset($tax_amount))
                                <input type="hidden" name="tax_amount" value="{{$tax_amount}}">
                                @endif
                                <input type="hidden" id="tax_record_id_it_code" name="tax_record_id_it_code" value="">
                                <button class="btn btn-success">Generate Vouchar</button>
                                <a href="{{route('accountant.index')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>


    </section>

</div>



@endsection


@push('custom-script')


<script>

    $(document).ready(function () {
        let it_code = $("#bank_vendor_id").find(':selected').data('it_code');
        let name = $("#bank_vendor_id").find(':selected').text();
        $("#bank_vendor_id").closest('tr').find('.it_code').text(it_code);
        
        $('#bank_id_tax').text(name);
        $('#bank_id_tax').closest('tr').find('.it_code').text(it_code);
    });

    $(function () {
        $("#data-table").DataTable();
    });

    $("#bank_vendor_id").change(function () { 
        let _this = $(this);
        let it_code = _this.find(':selected').data('it_code');
        let name = _this.find(':selected').text();
        _this.closest('tr').find('.it_code').text(it_code);
        
        $('#bank_id_tax').text(name);
        $('#bank_id_tax').closest('tr').find('.it_code').text(it_code);
    });
    
    $("#payable_tax_id").change(function () { 
        let _this = $(this);
        let it_code = _this.find(':selected').data('it_code');
        _this.closest('tr').find('.it_code').text(it_code);
    });
    $("#tax_record_id").change(function () { 
        let _this = $(this);
        let it_code = _this.find(':selected').data('it_code');
        _this.closest('tr').find('.it_code').text(it_code);
        $('#tax_record_id_it_code').val(it_code);
    });
   

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let form = _this.closest('form');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                form.submit();
            } 
            else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush

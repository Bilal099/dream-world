@extends('admin.layouts.app')

@push('custom-css')

<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    /* th{
        width: 50% !important;
    } */

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card  mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Show Vouchar Record</h3>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">    
                                    <table class="table table-striped " >
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>VCH</th>
                                                <th>Cheque Name</th>
                                                <th>Item Code</th>
                                                <th>Debit</th>
                                                <th>Credit</th>
                                                <th>Description</th>
                                                <th>Select Cheque</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data->voucharDetail as $key => $item)
                                            <tr>
                                                <td>{{@$data->vouchar_date}}</td>
                                                <td>{{@$item->VCH}}</td>
                                                <td>{{@$item->vendor->cheque_name}}</td>
                                                <td>{{$item->vendor->it_code}}</td>
                                                <td>{{number_format(@$item->total_debit)}}</td>
                                                <td>{{number_format(@$item->total_credit)}}</td>
                                                <td>{{@$item->ent_rem}}</td>
                                                @if (@$item->total_debit != null)
                                                    <form action="{{route('vouchar.chequePrint')}}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="cheque_name" value="{{@$item->vendor->cheque_name}}">
                                                        <input type="hidden" name="amount" value="{{@$item->total_debit}}">
                                                    <td>
                                                        <select class="select2 vendor_id" name="bank_cheque" id="">
                                                            <option value="">Select Option</option>
                                                            @foreach ($cheques as $detail)
                                                                <option value="{{@$detail->id}}" {{(@$detail->default==1)? 'selected':''}} >{{@$detail->bank_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        {{-- <a class="btn btn-danger btn-sm ml-1" href="{{route('vouchar.chequePrint',$item->id)}}" data-toggle="tooltip" data-placement="top" title="Cheque Print"> --}}
                                                            
                                                        {{-- </a> --}}
                                                        <button type="submit" class="btn btn-danger btn-sm ml-1"><i class="fas fa-book"></i> </button>
                                                    </td>
                                                </form>
                                            @else
                                                <td></td>
                                                <td></td>
                                            @endif
                                            </tr>
                                            @endforeach
                                            {{-- @foreach ($data->voucharDetail as $key => $item)
                                            <input type="hidden" name="vouchar_detail_id[]" value="{{@$item->id}}">
                                            @if ($key == 2)
                                            <tr>
                                                <th colspan="7" class="center">For Tax</th>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td>{{@$data->vouchar_date}}</td>
                                                <td>{{@$item->VCH}}</td>
                                                <td>
                                                    @if ($data->payment->vendor->id == $item->vendor_id)
                                                        <input type="hidden" name="vendor_id[]" value="{{$item->vendor_id}}">
                                                        {{@$data->payment->vendor->cheque_name}}
                                                    @else
                                                        <select class="select2 vendor_id" name="vendor_id[]" id="">
                                                            <option value="">Select Option</option>
                                                            @foreach ($vendors as $vendor)
                                                                <option value="{{@$vendor->vendor_id}}" data-it_code="{{@$vendor->vendor->it_code}}" {{(@$item->vendor_id==@$vendor->vendor_id)? 'selected':''}} >{{@$vendor->vendor->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </td>
                                                <td class="it_code">
                                                    

                                                    @if ($data->payment->vendor->id == $item->vendor_id)
                                                    <input type="hidden" name="it_code[]" value="{{@$data->payment->vendor->it_code}}">
                                                        {{@$data->payment->vendor->it_code}}
                                                    @else
                                                    <input type="hidden" name="it_code[]" value="{{@$item->vendor->it_code}}">
                                                        {{$item->vendor->it_code}}
                                                    @endif
                                                </td>
                                                <td>
                                                    <input type="hidden" name="total_debit[]" value="{{@$item->total_debit}}">
                                                    {{number_format(@$item->total_debit)}}
                                                </td>
                                                <td>
                                                    <input type="hidden" name="total_credit[]" value="{{@$item->total_credit}}">
                                                    {{number_format(@$item->total_credit)}}
                                                </td>
                                                <td>
                                                    <input type="text" name="description[]" value="{{@$item->ent_rem}}">
                                                </td>
                                            </tr>
                                            @endforeach --}}
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Narration</label>
                                        <input type="text" name="narration" id="narration" class="form-control" value="{{@$data->narration}}"  readonly>
                                    </div>
                                </div>
                            </div>
                            {{-- <table class="table table-striped table-bordered" >
                                <thead>
                                    <tr>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table> --}}
                        </div>
                        <div class="card-footer">
                            <a href="{{route('accountant.index')}}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>


            </div>
        </div>


    </section>

</div>



@endsection


@push('custom-script')


<script>
    $(function () {
        $("#data-table").DataTable();
    });

   

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let form = _this.closest('form');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                form.submit();
            } 
            else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush

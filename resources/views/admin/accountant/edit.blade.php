@extends('admin.layouts.app')

@push('custom-css')

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Update Supply Chain</h3>
                        </div>
                        @include('errors.messages')
                        <div class="card-body">
                            <form action="{{route('accountant.bulkUpdate')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if (isset($data))
                                    @foreach ($data as $item)
                                        <input type="hidden" name="id[]" value="{{ @$item->id }}">   
                                    @endforeach
                                @endif
                                <div class="card-body row">
                                   
                                    <div class="col-md-12 col-sm-12">
                                        <table class="table table-striped table-bordered table-responsive" style="margin-top:20px ">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>CHQ Name</th>
                                                    <th>NTN/CNIC Number</th>
                                                    <th>GL IT CODE  (A/Cs)</th>
                                                    <th>Nature Of Work</th>
                                                    <th>Purchaser Person </th>
                                                    <th>Total Amount</th>
                                                    <th>Category</th>
                                                    <th style="">Mode of Payment</th>
                                                    <th>Approved Amount </th>
                                                    <th>Approved by</th>
                                                    <th>Approve by Accountant</th>
                                                    {{-- <th>Action</th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data as $key => $item)
                                                <tr>
                                                    <td>
                                                        <div class="form-group @error('date.'.$item->id) has-error @enderror">
                                                            <input type="date" name="date[{{@$item->id}}]" class="" value="{{old('date.'.$item->id,@$item->date)}}" placeholder="" id="" >
                                                        </div>
                                                        @error('date.'.$item->id)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    
                                                    <td>
                                                        <div class="form-group @error('chq_name.'.$item->id) has-error @enderror">
                                                            <input type="text" name="chq_name[{{@$item->id}}]" class="" value="{{old('chq_name.'.$item->id,@$item->cheque_name)}}" placeholder="" id="" >
                                                        </div>
                                                        @error('chq_name.'.$item->id)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('cnic.'.$item->id) has-error @enderror">
                                                            <input type="text" name="cnic[{{@$item->id}}]" class="" value="{{old('cnic.'.$item->id,@$item->cnic_number)}}" placeholder="" id="" >
                                                        </div>
                                                        @error('cnic.'.$item->id)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('gt_item_code.'.$item->id) has-error @enderror">
                                                            <input type="text" name="gt_item_code[{{@$item->id}}]" class="" value="{{old('gt_item_code.'.$item->id,@$item->item_code)}}" placeholder="" id="" >
                                                        </div>
                                                        @error('gt_item_code.'.$item->id)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('nature_of_work.'.$item->id) has-error @enderror">
                                                            <input type="text" name="nature_of_work[{{@$item->id}}]" class="" value="{{old('nature_of_work.'.$item->id,@$item->work_of_nature)}}" placeholder="" id="" >
                                                        </div>
                                                        @error('nature_of_work.'.$item->id)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('purchaser_person.'.$item->id) has-error @enderror">
                                                            <input type="text" name="purchaser_person[{{@$item->id}}]" class="" value="{{old('purchaser_person.'.$item->id,@$item->purchaser_person)}}" placeholder="" id="" >
                                                        </div>
                                                        @error('purchaser_person.'.$item->id)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('total_amount.'.$item->id) has-error @enderror">
                                                            <input type="text" name="total_amount[{{@$item->id}}]" class="" value="{{old('total_amount.'.$item->id,@$item->total_amount)}}" placeholder="" id="" >
                                                        </div>
                                                        @error('total_amount.'.$item->id)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        {{-- <div class="form-group @error('category.'.$item->id) has-error @enderror">
                                                            <input type="text" name="category[{{@$item->id}}]" class="" value="{{old('category.'.$item->id,@$item->category)}}" placeholder="" id="" >
                                                        </div>
                                                        @error('category.'.$item->id)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror --}}
                                                        <div class="form-group @error('category.'.$item->id) has-error @enderror">
                                                            <select name="category[{{@$item->id}}]" id="category"  class="">
                                                                <option selected disabled>Select Option</option>
                                                                @foreach ($category as $obj)
                                                                    <option value="{{$obj->id}}" {{old('category.'.$item->id,@$item->category_id)==$obj->id? 'selected':''}}>{{$obj->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        @error('category.'.$item->id)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>

                                                    <td>
                                                        <div class="form-group">
                                                            <select name="mode_of_payment[{{@$item->id}}]" id="mode_of_payment"  class="form-control">
                                                                <option selected disabled>Select Option</option>
                                                                @foreach ($payment_type as $obj)
                                                                    <option value="{{$obj->id}}" {{old('mode_of_payment.'.$item->id,$item->payment_type_id)==$obj->id? 'selected':'' }}>{{$obj->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('mode_of_payment'.$item->id)
                                                            <p class="text-danger text-sm">{{$message}}</p>
                                                            @enderror
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="approved_amount[{{@$item->id}}]" class="" value="{{old('approved_amount.'.@$item->id,@$item->approved_amount_by_director)}}" placeholder="" id="" onkeypress="preventNonNumericalInput(event)">
                                                        @error('approved_amount'.$item->id)
                                                            <p class="text-danger text-sm">{{$message}}</p>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="approved_by[{{@$item->id}}]" id="approved_by"  class="form-control">
                                                                <option selected disabled>Select Option</option>
                                                                @foreach ($director as $obj)
                                                                    <option value="{{$obj->id}}" {{old('approved_by.'.$item->id,$item->director_id)==$obj->id? 'selected':'' }}>{{$obj->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('approved_by'.$item->id)
                                                            <p class="text-danger text-sm">{{$message}}</p>
                                                            @enderror
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="approved_amount_account[{{@$item->id}}]" class="" value="{{old('approved_amount_account.'.@$item->id,@$item->approved_amount_by_accountant)}}" placeholder="" id="" onkeypress="preventNonNumericalInput(event)">
                                                        @error('approved_amount_account'.$item->id)
                                                            <p class="text-danger text-sm">{{$message}}</p>
                                                        @enderror
                                                    </td>
                                                   
                                                </tr>
                                                @endforeach
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                                <button type="submit" class="btn btn-info">Update</button>
                                <a href="{{route('accountant.index')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>


@endsection


@push('custom-script')

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $("#add_detail").click(function(){  
        $('tbody').append($("#data_row").html());
    });

    $("table").on("click", ".deleteRow", function() {
        $(this).closest("tr").remove();
    });

    $('.chq_name_select').click(function (e) { 
        e.preventDefault();
        $('.chq_name_input').val($(this).html());
    });
   

</script>
@endpush

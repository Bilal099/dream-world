function page (path) {
  return () => import(/* webpackChunkName: '' */ `~/Pages/${path}`).then(m => m.default || m)
}

export default [
  { path: '/', name: 'welcome', component: page('Welcome.vue') },

  { path: '/login', name: 'login', component: page('Auth/Login.vue') },
  { path: '/register', name: 'register', component: page('Auth/Register.vue') },
  { path: '/password/reset', name: 'password.request', component: page('Auth/Password/Email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('Auth/Password/Reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('Auth/Verification/Verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('Auth/Verification/Resend.vue') },

  { path: '/home', name: 'home', component: page('home.vue') },
  {
    path: '/settings',
    component: page('Settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: page('Settings/Profile.vue') },
      { path: 'password', name: 'settings.password', component: page('Settings/Password.vue') }
    ]
  },

  { path: '*', component: page('Errors/404.vue') }
]

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\PermissionsController;
use App\Http\Controllers\Admin\MegaTagController;

use App\Http\Controllers\Admin\AjaxController;
use App\Http\Controllers\Admin\SupplyChainController;
use App\Http\Controllers\Admin\DirectorController;
use App\Http\Controllers\Admin\AccountantController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\PaymentTypeController;
use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\VendorController;
use App\Http\Controllers\Admin\BankController;
use App\Http\Controllers\Admin\BankAccountController;
use App\Http\Controllers\Admin\AccountCategoryController;
use App\Http\Controllers\Admin\CurAccountCategoryController;
use App\Http\Controllers\Admin\SubAccountCategoryController;
use App\Http\Controllers\Admin\VoucharController;
use App\Http\Controllers\Admin\DepartmentBankController;
use App\Http\Controllers\Admin\ChequeBankController;



use App\Http\Controllers\Admin\ModuleController;
use App\Http\Controllers\ForgetPasswordResetController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('contactUs', [ContactUsController::class,'index'])->name('contactUs.index');
// Route::post('contactUs/store', [ContactUsController::class,'AjaxCallForContactUs'])->name('AjaxCallForContactUs');

Route::get('/forgotpassword/{type}',            [ForgetPasswordResetController::class, 'viewForgotpassword'])                 ->name('forgotpassword');
Route::get('/resetPassword/{email_encode}',     [ForgetPasswordResetController::class, 'ResetPassword'])                      ->name('resetPassword'); // Forgot Password send email 
Route::post('/SetNewPassword',                  [ForgetPasswordResetController::class, 'SetNewPassword'])                     ->name('SetNewPassword'); // Set New Password 
Route::post('/AjaxCallForForgotPasswordEmail',  [ForgetPasswordResetController::class, 'AjaxCallForForgotPasswordEmail'])     ->name('AjaxCallForForgotPasswordEmail'); // Forgot Password send email 

// Route::get('/test', function () {
//     return "test";
// });

Route::get('/', [AuthController::class, 'loginShow']);

Route::prefix('admin')->group(function () {
    Route::get('/login',            [AuthController::class, 'loginShow'])       ->name('admin.login')           ->middleware('guest:admin');
    Route::get('/logout',           [AuthController::class, 'logout'])          ->name('admin.logout')          ->middleware('auth:admin');
    Route::post('/login',           [AuthController::class, 'loginPost'])       ->name('admin.login.store');
    Route::post('/passwordchange',  [AuthController::class, 'changePassword'])  ->name('admin.changePassword')  ->middleware('auth:admin');

    Route::middleware('auth:admin')->group(function () {
        // Custom Route
        Route::get('/',                     [HomeController::class,'index'])                ->name('admin.home');
        Route::get('user/permissions/{id}', [PermissionsController::class,'permissions'])   ->name('auth.user.permissions');
        Route::post('user/permissions',     [PermissionsController::class,'setPermissions'])->name('auth.user.permissions.post');  

        Route::post('supplyChain/bulkUpdate',[SupplyChainController::class,'bulkUpdate'])->name('supplyChain.bulkUpdate');
        Route::get('supplyChain/bulkEdit/{json_request}',[SupplyChainController::class,'bulkEdit'])->name('supplyChain.bulkEdit');
        Route::get('supplyChain/bulkPDF/{json_request}',[SupplyChainController::class,'bulkPDF'])->name('supplyChain.bulkPDF');
        Route::get('supplyChain/approve/{json_request}',[SupplyChainController::class,'bulkApprove'])->name('supplyChain.bulkApprove');
        Route::post('supplyChain/storeApproved',[SupplyChainController::class,'storeApproved'])->name('supplyChain.storeApproved');
        Route::get('supplyChain/supplyChainNotInProcessPagination',[SupplyChainController::class,'ajaxCallForSupplyChainNotInProcessPagination'])->name('supplyChain.notInProcess');
        Route::get('supplyChain/supplyChainCompletePagination',[SupplyChainController::class,'ajaxCallForSupplyChainCompletePagination'])->name('supplyChain.complete');


        Route::post('accountant/bulkUpdate',[AccountantController::class,'bulkUpdate'])->name('accountant.bulkUpdate');
        Route::get('accountant/approve/{json_request}',[AccountantController::class,'bulkApproveByAccountant'])->name('accountant.bulkApproveByAccountant');
        Route::post('accountant/storeApproved',[AccountantController::class,'storeApprovedByAccountant'])->name('accountant.storeApprovedByAccountant');

        // Ajax calls
        Route::post('vendorByDepartment', [AjaxController::class,'getVendorByDepartment'])->name('getVendorByDepartment');
        Route::post('vendorByName', [AjaxController::class,'getVendorByName'])->name('getVendorByName');
        Route::post('MainCategoryByDepartment', [CurAccountCategoryController::class,'getMainCategoryByDepartment'])->name('getMainCategoryByDepartment');
        Route::post('CurCategoryByDepartment', [SubAccountCategoryController::class,'getCurCategoryByDepartment'])->name('getCurCategoryByDepartment');
        Route::post('subCategoryByDepartment', [AjaxController::class,'getSubCategoryByDepartment'])->name('getSubCategoryByDepartment');
        Route::post('vendorDetail', [AjaxController::class,'getVendorDetail'])->name('getVendorDetail');
        Route::post('VendorBySubCategory', [AjaxController::class,'getVendorBySubCategory'])->name('getVendorBySubCategory');

        Route::get('accountCategory/import', [AccountCategoryController::class,'importView'])->name('mainCategory.importView');
        Route::post('accountCategory/import', [AccountCategoryController::class,'import'])->name('mainCategory.import');

        Route::get('curAccountCategory/import', [CurAccountCategoryController::class,'importView'])->name('curCategory.importView');
        Route::post('curAccountCategory/import', [CurAccountCategoryController::class,'import'])->name('curCategory.import');

        Route::get('subAccountCategory/import', [SubAccountCategoryController::class,'importView'])->name('subCategory.importView');
        Route::post('subAccountCategory/import', [SubAccountCategoryController::class,'import'])->name('subCategory.import');
        
        Route::get('vendor/import', [VendorController::class,'importView'])->name('vendor.importView');
        Route::post('vendor/import', [VendorController::class,'import'])->name('vendor.import');
        Route::get('vendor/vendorPagination',[VendorController::class,'ajaxCallForVendorPagination'])->name('vendor.pagination');

        Route::get('accountant/vouchar/{id}', [VoucharController::class,'createVouchar'])->name('vouchar.createVouchar');
        Route::get('accountant/vouchar/cheques/{id}', [VoucharController::class,'chequeBlade'])->name('vouchar.chequeBlade');
        // Route::get('accountant/vouchar/cheque/{id}', [VoucharController::class,'viewChequeDetail'])->name('vouchar.chequePrint');
        Route::post('accountant/vouchar/cheque', [VoucharController::class,'viewChequeDetail'])->name('vouchar.chequePrint');
        
        Route::resource('users',   UsersController::class);
        Route::resource('supplyChain', SupplyChainController::class);
        Route::resource('director', DirectorController::class);
        Route::resource('accountant', AccountantController::class);
        Route::resource('category', CategoryController::class);
        Route::resource('paymentType', PaymentTypeController::class);
        Route::resource('department', DepartmentController::class);
        Route::resource('vendor', VendorController::class);
        Route::resource('bank', BankController::class);
        Route::resource('bankAccount', BankAccountController::class);
        Route::resource('accountCategory', AccountCategoryController::class);
        Route::resource('curAccountCategory', CurAccountCategoryController::class);
        Route::resource('subAccountCategory', SubAccountCategoryController::class);

        Route::resource('accountant/vouchar', VoucharController::class);

        Route::resource('module', ModuleController::class);

        Route::resource('departmentBank', DepartmentBankController::class);
        Route::resource('cheque', ChequeBankController::class);
    });
});